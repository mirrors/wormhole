#include <cassert>
#include <iostream>
#include "http/request.h"

using namespace HTTP;

int main()
{
	Request request1{Request::Type::GET, "/test/:123/fast"};
	assert(( request1 == Request{Request::Type::GET, "/test/:123/fast"} ));
	assert(( request1 != Request{Request::Type::POST, "/test/:123/fast"} ));
	assert(( request1 != Request{Request::Type::GET, "/tes8203508/fast"} ));

	Request request1_match1{Request::Type::GET, "/test/the_data123/fast"};
	Request request1_match2{Request::Type::GET, "/test/notthedata/fast"};
						 assert(( (*request1.match_get_args(request1_match1))[0] == "the_data" ));
						 assert(( request1.match_get_args(request1_match2) == std::nullopt ));
	
	return 0;
}
