#include <cassert>
#include <iostream>
#include "crypt/rsa.h"

using namespace Crypt;

int main()
{
	RsaFactory factory{};
	Rsa key = factory.generate_key();

	std::cout << key.to_string();
	
	return 0;
}
