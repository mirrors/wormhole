#include <cassert>
#include <cmath>
#include <iostream>
#include "random.h"

int main()
{
	Random rng{};
	constexpr int start = 50;
	const char sym = rng.generate_token(1)[0];
	for (int i = 0; i < start; ++i)
	{
		std::cout << std::string((start-i), sym);
		std::cout << rng.generate_token(i);
		std::cout << rng.generate_token(std::abs(i));
		std::cout << std::string((start-i), sym) << '\n';

	}

	std::cout << std::endl;
	return 0;
}
