/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include <cstring>
#include <algorithm>
#include "random.h"

Random::Random()
	: rng()
{
}

std::string Random::generate_token(size_t len)
{
	using engine = std::mt19937;
	const char* syms = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_-!@";
	static size_t syms_len = std::strlen(syms);
	engine r(rng());

	std::uniform_int_distribution<engine::result_type> dist{0,
						// I love C++
						static_cast<engine::result_type>(syms_len - 1)};
	std::string result{};
	result.resize(len);
	std::generate_n(std::begin(result), len, [&, syms](){ return syms[dist(rng)]; });
	return result;
}
