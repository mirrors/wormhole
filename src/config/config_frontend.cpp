/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include "config_frontend.h"

// Decl
namespace
{
	bool load_fcgi(Config::Frontend&, YAML::Node&);
}

void Config::load_frontend(Config::Frontend& cfg, YAML::Node& node)
try {
	if (!node["frontend"]) return;
	
	// Check Hint first
	try {
		if (node["frontend"]["fe_type"])
		{
			std::string fe_type = node["frontend"]["fe_type"].as<std::string>();
			if (fe_type == "fcgi")
	cfg.type = ::Frontend::Type::FCGI;
			else if (fe_type == "static")
	cfg.type = ::Frontend::Type::FCGI;
		}
	} catch(const YAML::BadConversion& err) {}

	// If still not found...
	if (cfg.type == ::Frontend::Type::UNSET)
	{
		try {
			if (node["frontend"]["fcgi"]) cfg.type = ::Frontend::Type::FCGI;
			if (node["frontend"]["static"]) cfg.type = ::Frontend::Type::STATIC;
		} catch(const YAML::BadConversion& err) {}
	}
		
	switch (cfg.type)
	{
	case ::Frontend::Type::FCGI: load_fcgi(cfg, node); break;
	case ::Frontend::Type::STATIC: break; // Not ready
	default: break;
	}
}
catch(const YAML::BadConversion& err)
{}

namespace
{
	bool load_fcgi(Config::Frontend& cfg, YAML::Node& node)
	{
		if (node["frontend"]["fcgi"]["exec"])
			cfg.exec = node["frontend"]["fcgi"]["exec"].as<std::string>();

		return true;
	}
}



