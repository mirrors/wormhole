/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include <string>
#include <yaml-cpp/yaml.h>
#include <cstdint>

namespace Config
{
	struct HTTP
	{
		uint16_t port{8080};
		uint16_t pool_size{8};
	};

	void load_http(Config::HTTP& cfg, YAML::Node& node);
}

