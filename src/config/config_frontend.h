/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include <string>
#include <yaml-cpp/yaml.h>
#include <cstdint>
#include "frontends/types.h"
#include "database/database.h"

namespace Config
{
	struct Frontend
	{
		::Frontend::Type type{::Frontend::Type::UNSET};
		std::string exec{};
	};

	void load_frontend(Config::Frontend& cfg, YAML::Node& node);
}
