/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include <yaml-cpp/yaml.h>
#include <string>

namespace Config
{
	struct Instance
	{
		std::string host{"localhost"};
	};

	void load_instance(Config::Instance& cfg, YAML::Node& node);
}
