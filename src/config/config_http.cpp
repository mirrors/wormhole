/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include "config_http.h"
#include <yaml-cpp/exceptions.h>

void Config::load_http(Config::HTTP& cfg, YAML::Node& node)
try {
	cfg.port = node["http"]["port"].as<unsigned>();
	cfg.pool_size = node["http"]["thread_pool_size"].as<unsigned>();
}
catch(const YAML::BadConversion& err)
{}

