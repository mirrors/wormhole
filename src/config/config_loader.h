/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include <logger.h>
#include <filesystem>
#include <yaml-cpp/yaml.h>
#include <optional>
#include "config_db.h"
#include "config_http.h"
#include "config_instance.h"
#include "config_frontend.h"

namespace Config
{
	class ConfigLoader
	{
	public:
		ConfigLoader();
		~ConfigLoader() = default;

		void load_config(const std::filesystem::path& path);

		/**
		 * @brief List of commonly used configuration options
		 */
		struct CommonConfig
		{
			Config::HTTP http;
			Config::Database database;
			Config::Instance instance;
			Config::Frontend frontend;
		} config;

	private:
		Logger::category_id cat_id;
	};

	// Global instance of config
	ConfigLoader& instance();
}
