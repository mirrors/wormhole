/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include "config_loader.h"
#include "logger.h"

using namespace std::string_literals;
using namespace Config;

namespace
{
	// Global instance
	ConfigLoader inst{};
}

ConfigLoader& Config::instance()
{
	return inst;
}

ConfigLoader::ConfigLoader()
	: cat_id{ Logger::make_category("Config") }
{}

void ConfigLoader::load_config(const std::filesystem::path& path)
	try
	{
		// Make sure it's loaded
		YAML::Node node = YAML::LoadFile(path);
	
		Config::load_http(config.http, node);
		Config::load_database(config.database, node);
		Config::load_instance(config.instance, node);
		Config::load_frontend(config.frontend, node);
	}
	catch (const YAML::BadFile& err) {
		Logger::log(
			cat_id,
			Logger::Level::ERROR,
			"Couldn't load config: \""s + path.string() + "\"");
		
		throw err;
	}
	catch (const YAML::BadConversion& err)
	{
		// NOOP
	}

