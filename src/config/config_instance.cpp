/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include "config_instance.h"

void Config::load_instance(Config::Instance& cfg, YAML::Node& node)
try {
	cfg.host = node["instance"]["host"].as<std::string>();
}
catch(const YAML::BadConversion& err)
{}
