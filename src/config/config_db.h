/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include "database/database.h"
#include <string>
#include <yaml-cpp/yaml.h>
#include <cstdint>

namespace Config
{
	struct Database
	{
		std::string name{"wormhole"};
		/** SQLite only */
		std::string file{"wormhole.sqlite"};
		DB::Database::Type type{DB::Database::Type::Unset};
	};

	void load_database(Config::Database& cfg, YAML::Node& node);
}

