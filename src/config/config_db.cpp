/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include "config/config_db.h"

// Decl
namespace
{
	bool load_database_sqlite(Config::Database&, YAML::Node&);
}

// Calls other (local) database functions if the database block exists
void Config::load_database(Config::Database& cfg, YAML::Node& node)
try {
	cfg.name = node["database"]["db_name"].as<std::string>();

	load_database_sqlite(cfg, node);
}
catch(const YAML::BadConversion& err)
{}

namespace
{
	bool load_database_sqlite(Config::Database& cfg, YAML::Node& node)
	{
		// If database block doesn't exist, give up anyway
		if (node["database"]["db_type"].as<std::string>() != "sqlite")
			return false;

		cfg.type = DB::Database::Type::SQLite;
		cfg.file = node["database"]["sqlite"]["db_file"].as<std::string>();
		return true;
	}
}
