/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include <cstdlib>
#include <stdexcept>
#include <utility>
#include <iostream>
#include <functional>
#include <string.h>
#include <string_view>
#include <iomanip>
#include <unistd.h>
#include <getopt.h>
#include "control.h"
#include "database/database.h"
#include "types/user.h"
#include <array>

namespace {
	constexpr int CMD_GAP = 27; 
}

using CTLFunction_t = std::function<int(int argc, char** argv)>;

struct CTLOptions
{
	const std::string name;
	const std::string desc;
	CTLFunction_t func;
};

void print_usage(const std::string& command, std::ostream& buf)
{
	buf << "Usage: " << command << " [command] [command-args]...\n";
}

namespace CTL {
	int create_user(int argc, char** argv)
	{
		option long_options[] = {
			{ "name", required_argument, 0, 'n' },
			{ "email", optional_argument, 0, 'e' },
			{ "nickname", optional_argument, 0, 'N' },
			{ 0, 0, 0, 0 }
		};
		
		User props{};
		props.email = "";
		props.local = true;
		props.birthday = 0;

		// Loop until break
		int option_idx = 0;

		for (int i = 0; true; ++i)
		{
			int rc = getopt_long(argc - 1, argv + 1, "n:eN",
		 long_options, &option_idx);

			switch (rc)
			{
			case 'n': if (optarg) { props.acct = optarg; } break;
			case 'N': if (optarg) { props.display_name = optarg; } break;
			case 'e': if (optarg) { props.email = optarg; } break;
	
			default: // -1
	// If i is 0, this is the first iteration, so nothing matched
	if (i == 0)
	{
		print_usage(argv[0], std::cout);
		std::cout << "\ncreate_user:\n" << std::left
		 << std::setw(CMD_GAP) << "  -n, --name" << std::setw(0) << "Username (required)\n"
		 << std::setw(CMD_GAP) << "  -N, --nickname" << std::setw(0) << "Display name\n"
		 << std::setw(CMD_GAP) << "  -e, --email" << std::setw(0) << "Email" << std::endl;
		return EXIT_FAILURE;
	}
	// Fall?
	goto out; // Break out of this loop
			}
		}
	out:
		auto db = DB::load_db_from_cfg();

		if (props.display_name.empty() && !props.acct.empty())
			props.display_name = props.acct;

		db->create_user(props);
		
		return 0;
	}

	int delete_user(int argc, char** argv)
	{
		
		return 0;
	}
	
	int list_user(int argc, char** argv)
	{
		option long_options[] = {
			{ "acct", optional_argument, 0, 'a' },
			{ "id", optional_argument, 0, 'i' },
			{ 0, 0, 0, 0 }
		};

		decltype(User::id) id = 0;
		decltype(User::acct) acct;

		// Loop until break
		int option_idx = 0;

		for (int i = 0; true; ++i)
		{
			int rc = getopt_long(argc - 1, argv + 1, "a:i:",
		 long_options, &option_idx);

			switch (rc)
			{
			case 'a': if (optarg) { acct = optarg; } break;
			case 'i':
	try
	{
		if (optarg) {
			id = std::stol(optarg);
		}
		break;
	}
	catch (const std::invalid_argument err)
	{ /* Fall through */ }

	// Might fall?
			case -1:
			default:
	// If i is 0, this is the first iteration, so nothing matched
	if (i == 0)
	{
		print_usage(argv[0], std::cout);
		std::cout << "\nlist_user:\n" << std::left 
		 << std::setw(CMD_GAP) << "  -a, --acct" << std::setw(0) << "Lookup by account\n" << std::setw(0)
		 << std::setw(CMD_GAP) << "  -i, --id" << std::setw(0) << "Lookup by id" << std::endl;
		return EXIT_FAILURE;
	}
	// Fall?
	goto out; // Break out of this loop
			}
		}
	out:
		auto db = DB::load_db_from_cfg();

		User result;

		if (!acct.empty())
			result = db->get_user(acct);
		else if (id != 0)
			result = db->get_user(id);

		std::cout << result << std::endl;;
		
		return 0;
	}

	int lusers_local(int argc, char** argv)
	{
		return 0;
	}
}

int control_mode_init(int argc, char** argv)
{
	ArgvVector_t args(argv, argv + argc);
	
	// Put commands in here as CTLOptions struct
	const std::array<CTLOptions, 4> commands = {
		CTLOptions { "create_user", "Creates a user", CTL::create_user },
		CTLOptions { "delete_user", "Deletes a user", CTL::delete_user },
		CTLOptions { "list_user", "Displays a local user", CTL::list_user },
		CTLOptions { "lusers_local", "Lists all local users", CTL::lusers_local },
	};

	// Now do the thing
	for (const CTLOptions& cmd : commands)
	{
		if (args[1] == cmd.name)
			return cmd.func(argc, argv);
	}

	// Command doesn't exist, display help message
	print_usage(args[0], std::cout);
	std::cout << "\n" <<
		"Commands available:\n";

	size_t l = commands.size();
	for (const CTLOptions& cmd : commands)
	{
		std::cout << "  - " << std::left << std::setw(24) << cmd.name
	  << std::setw(0) << cmd.desc << (--l != 0 ? "\n" : "");
	}

	// Flush and leave
	std::cout << std::endl;

	return EXIT_FAILURE;
}
