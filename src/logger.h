/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

// Note: I copied this OOP garbage from another project

// Just your generic, average logger
#ifndef LOGGER_H
#define LOGGER_H

#include <functional>
#include <vector>
#include <chrono>
#include <string>
#include <sstream>
#include <iterator>
#include <cstddef>

#define RED "\x1B[31m"
#define GREEN "\x1B[32m"
#define YELLOW "\x1B[33m"
#define BLUE "\x1B[34m"
#define MAGENTA "\x1B[35m"
#define CYAN "\x1B[36m"
#define WHITE "\x1B[37m"
#define RESET "\x1B[0m"

namespace Logger
{

	using category_id = int;

	static const std::string level_string[] = {
		"Debug",
		"Info",
		"Warn",
		"Error"
	};
	
	enum class Level
	{
		DEBUG,
		INFO,
		WARN,
		ERROR
	};

	/**
	 * Creates a category.
	 *
	 * @return ID of this category, remember it!
	 */
	category_id make_category(std::string name);

	/**
	 * Creates a log onto the console and possibly elsewhere.
	 *
	 * There are 3 overloaded functions for ease-of-use
	 *
	 * @param category Category created by @function make_category.
	 *  Can be -1 for no category, but see the overloaded function.
	 * @param lvl The Log level.
	 * @param message The message of the log.
	 */
	void
	log(int category,
		Logger::Level lvl,
		std::string const& message);

	/**
	 * Creates a log without a category.
	 *
	 * @param lvl Log level.
	 * @param message The log message.
	 */
	inline void
	log(Logger::Level lvl,
		std::string const& message)
	{
		Logger::log(-1, lvl, message);
	}

	/**
	 * Creates a log message of the INFO level.
	 *
	 * @param message The log message.
	 */
	inline void
	log(std::string const& message)
	{
		Logger::log(Level::INFO, message);
	}
}

#endif // LOGGER_H
