/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include <iostream>
#include <array>
#include <string>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <cassert>
#include <sys/select.h>
#include <unistd.h>
#include "fcgi.h"
#include "config/config_loader.h"
#include "logger.h"
#include "route_args.h"

using namespace std::string_literals;

constexpr int FCGI_LISTENSOCK_FILENO = STDIN_FILENO;
constexpr int FCGI_HEADER_LEN = 8;
constexpr int FCGI_VERSION_1 = 1;

struct FCGI_Header {
	unsigned char version;
	unsigned char type;
	unsigned char requestIdB1;
	unsigned char requestIdB0;
	unsigned char contentLengthB1;
	unsigned char contentLengthB0;
	unsigned char paddingLength;
	unsigned char reserved;
};

enum FCGI_TYPE {
	FCGI_BEGIN_REQUEST = 1,
	FCGI_ABORT_REQUEST,
	FCGI_END_REQUEST,
	FCGI_PARAMS,
	FCGI_STDIN,
	FCGI_STDOUT,
	FCGI_STDERR,
	FCGI_DATA,
	FCGI_GET_VALUES,
	FCGI_GET_VALUES_RESULT,
	FCGI_UNKNOWN_TYPE,
};

/*
 * Value for requestId component of FCGI_Header
 */
#define FCGI_NULL_REQUEST_ID	 0

typedef struct {
	unsigned char roleB1;
	unsigned char roleB0;
	unsigned char flags;
	unsigned char reserved[5];
} FCGI_BeginRequestBody;

typedef struct {
	FCGI_Header header;
	FCGI_BeginRequestBody body;
} FCGI_BeginRequestRecord;

/*
 * Mask for flags component of FCGI_BeginRequestBody
 */
#define FCGI_KEEP_CONN  1

/*
 * Values for role component of FCGI_BeginRequestBody
 */
#define FCGI_RESPONDER  1
#define FCGI_AUTHORIZER 2
#define FCGI_FILTER	 3

struct FCGI_EndRequestBody {
	unsigned char appStatusB3;
	unsigned char appStatusB2;
	unsigned char appStatusB1;
	unsigned char appStatusB0;
	unsigned char protocolStatus;
	unsigned char reserved[3];
};

struct FCGI_EndRequestRecord {
	FCGI_Header header;
	FCGI_EndRequestBody body;
};

/*
 * Values for protocolStatus component of FCGI_EndRequestBody
 */
#define FCGI_REQUEST_COMPLETE 0
#define FCGI_CANT_MPX_CONN	1
#define FCGI_OVERLOADED	2
#define FCGI_UNKNOWN_ROLE	 3

/*
 * Variable names for FCGI_GET_VALUES / FCGI_GET_VALUES_RESULT records
 */
#define FCGI_MAX_CONNS  "FCGI_MAX_CONNS"
#define FCGI_MAX_REQS   "FCGI_MAX_REQS"
#define FCGI_MPXS_CONNS "FCGI_MPXS_CONNS"

struct FCGI_UnknownTypeBody {
	unsigned char type;	
	unsigned char reserved[7];
};

struct FCGI_UnknownTypeRecord {
	FCGI_Header header;
	FCGI_UnknownTypeBody body;
};

namespace {
	static std::array<int, 6> fcgi_fd{};
	static int logger_cat = Logger::make_category("FCGI");

	enum fcgi_fd_pipes
	{
		STDIN_RD,
		STDIN_WR,
		STDOUT_RD,
		STDOUT_WR,
		STDERR_RD,
		STDERR_WR,
	};

	// Helper function for reading a pipe
	int selector(const std::vector<int>& fds)
	{
		assert(fds.size() != 0);
		int max_fd = fds[0];
		fd_set rfds;
		FD_ZERO(&rfds);
		for (int fd: fds)
		{
			FD_SET(fd, &rfds);
			if (fd > max_fd)
	max_fd = fd;
		}


		int rc = select(max_fd+1, &rfds, nullptr, nullptr, nullptr);
		if (rc == -1)
		{
			Logger::log(
	logger_cat,
	Logger::Level::ERROR,
	"select(): "s + std::strerror(errno));
		}

		return rc;
	}
	
	void setup_routes(HTTP::Server* server)
	{
		// TODO Use bitmasks for this, fucking hell...
		server->add_route({{HTTP::Request::Type::GET, "/:"}, Route::fcgi_request});
		server->add_route({{HTTP::Request::Type::PUT, "/:"}, Route::fcgi_request});
		server->add_route({{HTTP::Request::Type::POST, "/:"}, Route::fcgi_request});
		server->add_route({{HTTP::Request::Type::DELETE, "/:"}, Route::fcgi_request});
		server->add_route({{HTTP::Request::Type::PATCH, "/:"}, Route::fcgi_request});
		server->add_route({{HTTP::Request::Type::OTHER, "/:"}, Route::fcgi_request});
	}
}

/* When the user sends a request, serialize and pass it to the running process */
HTTP::Response Route::fcgi_request(std::any& args, const HTTP::Request& req, const HTTP::RequestArgs_t& arg)
{
	constexpr size_t READ_BUFFER_SIZE = 65535;
	// FCGI Request number for application
	static int reqNum = 0;
	++reqNum;
	
	DESTRUCT_WORMHOLE_ARGS(args);

	// TODO
	//write(fcgi_fd[STDIN_WR], "test", 4);

	// Wait for data
	selector({ fcgi_fd[STDOUT_RD] });
	
	char* buf = new char[READ_BUFFER_SIZE];
	// *shrugs* could probably do an overload here
	int sz = read(fcgi_fd[STDOUT_RD], buf, READ_BUFFER_SIZE);
	std::string res_data{buf, buf + sz};
	delete[] buf;
	
	return HTTP::Response{ res_data, HTTP::MIME::HTML };
}

void Frontend::init_fcgi(HTTP::Server* server)
{
	if (Config::instance().config.frontend.type != Type::FCGI)
		return;

	setup_routes(server);
}

void Frontend::poll_fcgi_from_process()
{
	int rc;
	int size;
	int buf[4096];

	do
	{
		// TODO
		rc = selector({ fcgi_fd[STDIN_RD] });

		std::cout << "Data get!" << std::endl;;
		size = read(rc, buf, 1);
	}
	while (rc != -1);
}

void Frontend::fork_fcgi_process()
{
	if (Config::instance().config.frontend.type != Type::FCGI)
		return;
	
	const std::string& exec = Config::instance().config.frontend.exec;

	pipe(fcgi_fd.data());
	pipe(fcgi_fd.data() + 2);
	pipe(fcgi_fd.data() + 4);
	
	int pid = fork();
	bool is_parent = pid != 0;
	if (is_parent)
	{
		Logger::log(
			logger_cat,
			Logger::Level::INFO,
			"Executing frontend \"" + exec + "\" in process " + std::to_string(pid) + "...");
		return;
	}

	// We are the child process from here onwards

	// FCGI says these aren't open
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
	close(FCGI_LISTENSOCK_FILENO);
	if (dup2(fcgi_fd[STDIN_WR], FCGI_LISTENSOCK_FILENO) == -1)
	{
		perror("dup2");
	}
	if (dup2(fcgi_fd[STDOUT_WR], STDOUT_FILENO) == -1)
	{
		perror("dup2");
	}
	if (dup2(fcgi_fd[STDOUT_WR], STDERR_FILENO) == -1)
	{
		perror("dup2");
	}

	char* argv[] = {
		nullptr
	};

	int rc;
	rc =
#ifdef _GNU_SOURCE
			// Checks PATH as well
	execvpe
#else
	execve
#endif
		(exec.data(), argv, nullptr);
	
	exit(0);
}
