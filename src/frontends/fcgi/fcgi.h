/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include <unistd.h>
#include "http/httpserver.h"

namespace Route
{
	HTTP::Response fcgi_request(std::any& args, const HTTP::Request& req, const HTTP::RequestArgs_t& arg);
}

namespace Frontend
{
	void init_fcgi(HTTP::Server* server);
	void poll_fcgi_from_process();

	void fork_fcgi_process();
}
