/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

namespace Frontend
{
	enum class Type
	{
		UNSET,
		FCGI,
		STATIC,
	};
}
