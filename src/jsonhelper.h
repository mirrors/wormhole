/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once
#include "http/request.h"
#define RAPIDJSON_HAS_STDSTRING 1
#define RAPIDJSON_NAMESPACE rjson
#define RAPIDJSON_NAMESPACE_BEGIN namespace rjson {
#define RAPIDJSON_NAMESPACE_END   }

#include <rapidjson/document.h>
#include <rapidjson/rapidjson.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>
#include "http/httpserver.h"

#define RJSON_OPTIONAL(ne, o) if (!(ne).empty()) { o; }

namespace rjson
{
	std::string to_string(rjson::Document& doc);
}

/**
 * @brief Fix-up arguments from the JSON data in request
 *
 * This is used by Pleroma and some of the frontends. They send JSON
 * instead of standard POST requests, but for compliance, this isn't
 * forced to all POST requests (i.e. Webfinger)
 *
 * @param req Request to get from
 * @param arg Argument that will get modified (if no exception thrown)
 * @return true if modified, false if the same.
 */
bool try_load_args_from_json(HTTP::Request& req);
