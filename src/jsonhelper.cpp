/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include "jsonhelper.h"

std::string rjson::to_string(rjson::Document& doc)
{
	rjson::StringBuffer buf;
	rjson::Writer<rjson::StringBuffer> writer(buf);
	doc.Accept(writer);
	// Make copy into string on return
	return buf.GetString();
}

#include <iostream>
bool try_load_args_from_json(HTTP::Request& req)
{
	rjson::Document doc;
	if (!req.data.empty())
	{
		rjson::ParseResult rc = doc.Parse(req.data.data());
		if (!rc)
			return false;
	}
	else return false;
	
	for (auto itr = doc.MemberBegin(); itr != doc.MemberEnd(); ++itr)
	{
		req.param.insert({ itr->name.GetString(), std::string(itr->value.GetString()) });
	}
	return true;
}
