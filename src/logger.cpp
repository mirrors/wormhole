/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include <iomanip>
#include <iostream>
#include <istream>
#include <sstream>
#include <utility>
#include <vector>
#include <ctime>
#include "logger.h"

namespace { std::vector<std::string> categories; }

// Category is made
Logger::category_id
Logger::make_category(std::string name)
{
	int id = ::categories.size();
	::categories.push_back(name);
	return id;
}

static std::string
pretty_time(const std::string& format)
{
	using clock_type = typename std::chrono::system_clock;
	const std::chrono::time_point<clock_type>
		time_point;

	std::stringstream time_str;
	const std::time_t c_time = clock_type::to_time_t(time_point);
	// time_t doesn't specify it's output, so write into string stream and return that
	time_str << std::put_time(std::localtime(&c_time), format.c_str());

	return time_str.str();
}


void
Logger::log(Logger::category_id id,
			Logger::Level lvl,
			std::string const& message)
{
	std::stringstream s;

	s << "[" << YELLOW <<  pretty_time("%T") << RESET;

	// No category
	if (id != -1)
		s << "|" << CYAN << categories.at(id);
	
	s << RESET << "] <" << Logger::level_string[static_cast<int>(lvl)] << "> " <<
		message;
	
	if (lvl == Logger::Level::WARN)
	{
		std::cerr << s.rdbuf() << std::endl;
	}
	else {
		std::cout << s.rdbuf() << std::endl;
	}
}



