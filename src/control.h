/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include <vector>
#include <string>

/* Control mode (aka. ctl mode) is a parameter controlled
 * mode where the user can configure, create users, manipulate options,
 * test things, get information, and much more about an instance while it (probably)
 * isn't running.
 */

using ArgvVector_t = const std::vector<std::string>;

int control_mode_init(int argc, char** argv);
