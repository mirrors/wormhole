/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include <string>
#include <cstddef>
#include <random>

class Random
{
public:
	Random();
	~Random() = default;

	/**
	 * @brief Generates a random token-like value, mainly for OAuth.
	 * @param len Length of token
	 * @return [a-zA-Z0-9_!-@] token of size len
	 */
	std::string generate_token(size_t len);
private:
	std::random_device rng;
};
