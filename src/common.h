/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#define HINT_UNUSED(var) (void)(var);

// NOOP for now!
