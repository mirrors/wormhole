/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once
#include <thread>
#include <vector>

namespace Threads
{
	class ThreadPool
	{
		struct ThreadInfo
		{
			int children; /// Represents how many "members" in a pool
			std::thread thr;
		};
	public:
		using threadpool_t = std::vector<ThreadInfo>;

		ThreadPool(size_t size);
		~ThreadPool() = default;

		template <class Function, class... Args> 
		void start(Function&& f, Args&&... args)
		{
			for (size_t i = 0; i < pool.size(); ++i)
			{
	pool[i].thr = std::thread{f, &args..., i};
			}
		}

		/// Gets a pool with the least amount of children ("smallest" thread)
		inline ThreadPool::ThreadInfo& select_pool() { return *it_smallest_pool; }

		inline size_t get_size() const noexcept { return pool.size(); }
	private:
		/// Only calculate the smallest pool when new pools are added
		void set_smallest_pool();
		
		threadpool_t pool;
		threadpool_t::iterator it_smallest_pool;
	};
}
