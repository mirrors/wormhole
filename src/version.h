/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#ifndef WH_VERSION_H
#define WH_VERSION_H

#include <string_view>

namespace Wormhole
{
	constexpr std::string_view version_string = "Wormhole development build";
}

#endif // WH_VERSION_H
