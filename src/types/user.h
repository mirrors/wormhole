/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include <string>
#include "crypt/rsa.h"

class User
{
public:
	User();
	~User() = default;
	// Values read from databases, so public here
	int id;
	std::string email;
	bool local;
	std::string bio;
	std::string acct;
	int birthday;
	std::string display_name;
	std::string key;
	
	inline const std::string& get_key() const { return key; }
	
	/** @brief Generates an RSA key, retrieve with get_key() */
	void generate_key(const Crypt::RsaFactory& keygen);

	inline operator std::string() const
	{
		return acct;
	}

	friend std::ostream& operator<<(std::ostream& os, const User& user);
};

std::ostream& operator<<(std::ostream& os, const User& user);
