/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include <string>
#include <array>
#include "languages.h"
#include "random.h"

struct Config_t
{
	struct
	{
		std::string domain;
		std::string name;
		std::string description;
		std::string logo;
		std::string admin_email;
		std::string background_image;
		std::string thumbnail;
		std::array<Language, LANG_SIZE> languages;
	} instance;

	struct
	{
		bool approval_required;
		bool registrations_enabled;
	} account;

	struct
	{
		unsigned avatar;
		unsigned media;
	} upload_limit;
	
	void generate_tokens(Random& rng);
};
