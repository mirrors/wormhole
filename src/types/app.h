/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include <string>
#include "random.h"

struct App
{
	App() = default;
	~App() = default;

	int id;
	std::string client_name;
	std::string client_id;
	std::string client_secret;
	std::string app_website;

	void generate_tokens(Random& rng);
};
