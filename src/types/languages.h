/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#ifndef LANGUAGES_H
#define LANGUAGES_H

enum Language
{
	EN_US,
	LANG_SIZE,
};

#endif // LANGUAGES_H
