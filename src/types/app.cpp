/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include "app.h"

void App::generate_tokens(Random& rng)
{
	constexpr size_t token_size = 48;
	client_id = rng.generate_token(token_size);
	client_secret = rng.generate_token(token_size);
}
