/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include <iomanip>
#include "user.h"

User::User()
	: id(0), email(""), local(false), bio(""), acct(""),
	  birthday(0), display_name(""), key("")
{}

void User::generate_key(const Crypt::RsaFactory& keygen)
{
	auto key = keygen.generate_key();

	this->key = key.to_string();
}

std::ostream& operator<<(std::ostream& os, const User& user)
{
	using namespace std;
	using namespace std::string_literals;
	constexpr int gap = 16;
	constexpr const char* gap_sym = " | ";

	os << " User \"" << user.acct << "\":\n";
	os << right<<setw(gap) << "id" << gap_sym << left<<setw(0) << user.id << "\n";
	os << right<<setw(gap) << "display name" << gap_sym << left<<setw(0) << user.display_name << "\n";
	os << right<<setw(gap) << "email" << gap_sym << left<<setw(0) << user.email << "\n";
	os << right<<setw(gap) << "local" << gap_sym << left<<setw(0) << user.local << "\n";
	os << right<<setw(gap) << "bio" << gap_sym << left<<setw(0) << user.bio << "\n";
	os << right<<setw(gap) << "key" << gap_sym << left<<setw(0) << user.get_key();
	
	return os;
}
