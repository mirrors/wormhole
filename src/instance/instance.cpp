/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include "instance.h"
#include "config/config_loader.h"

using namespace std::string_literals;

const std::string& Instance::instance_host_https(bool append_slash /* = false */)
{
	static const std::string inst = "https://"s + Config::instance().config.instance.host;
	static const std::string inst_slashed = inst + "/";
	return append_slash ? inst_slashed : inst;
}

