/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include "config/config_loader.h"
#include <string_view>

namespace Instance
{
	const std::string& instance_host_https(bool append_slash = false);
}
