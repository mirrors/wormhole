/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include <utility>
#include "http/mime.h"
#include "response.h"

using namespace HTTP;

Response::Response(std::vector<unsigned char> data,
	   MIME::mime_t mimetype,
	   HTTP::Code code /* = HTTP::Code::OK*/)
	: data{std::move(data)},
	  mimetype{mimetype},
	  code{code}
{}

Response::Response(const std::string& str)
	: Response(str, HTTP::MIME::JSON)
{}

Response::Response(const std::string& str,
	   MIME::mime_t mimetype,
	   HTTP::Code code /* = HTTP::Code::OK*/)
	: data{str.begin(), str.end()},
	  mimetype{mimetype},
	  code{code}
	  
{}
