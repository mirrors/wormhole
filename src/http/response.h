/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include <string>
#include <vector>
#include <string_view>
#include <map>
#include "mime.h"
#include "responsecode.h"
#include "stringvariant.h"

namespace HTTP
{
	class Response
	{
	public:
		/** Initialize from vector of unsigned chars */
		Response(std::vector<unsigned char> data,
	 MIME::mime_t mimetype,
	 HTTP::Code code = HTTP::Code::OK);
		
		/** Initialize from string */
		Response(const std::string& str);
		Response(const std::string& str,
	 MIME::mime_t mimetype,
	 HTTP::Code code = HTTP::Code::OK);
		~Response() = default;
		
		const std::vector<unsigned char> data;
		MIME::mime_t mimetype;
		/// @see responsecode.h
		HTTP::Code code;
	};
}
