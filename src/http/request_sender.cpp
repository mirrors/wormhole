/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include "request_sender.h"

using namespace HTTP;

RequestSender::RequestSender()
	: curl{curl_multi_init(), ::curl_multi_cleanup}
{}

void RequestSender::request(const HTTP::Request& request) const
{
	std::unique_ptr<CURL, decltype(&curl_easy_cleanup)>
		curl_req{curl_easy_init(), curl_easy_cleanup};

	curl_easy_setopt(curl_req.get(), CURLOPT_URL, request.get_url().data());

	// Set appropriate type
	switch (request.get_type())
	{
	case HTTP::Request::Type::GET:
		curl_easy_setopt(curl_req.get(), CURLOPT_HTTPGET, 1);
		break;
	case HTTP::Request::Type::POST:
		// Does this really offer any advantages? (see below code)
		curl_easy_setopt(curl_req.get(), CURLOPT_POST, 1);
		break;
	default:
		curl_easy_setopt(curl_req.get(), CURLOPT_CUSTOMREQUEST, request.get_type_string());
		break;
	}
	
	// TODO keep going
}
