/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include "httpserver.h"
#include "http/request.h"
#include <cstdint>
#include <iostream>

using namespace HTTP;

Server::Server(uint16_t port, std::any callback_args)
	: port{port},
	  req_map(),
	  cb_args(callback_args),
	  logger_cat( Logger::make_category("HTTP") )
{}

void Server::start()
{
	std::cerr << "[DEV BUG] Notice: Using base (HTTP)Server::start(), should be inherited" << std::endl;
}

void Server::stop()
{
	std::cerr << "[DEV BUG] Notice: Using base (HTTP)Server::stop(), should be inherited" << std::endl;
}

void Server::add_routes(std::initializer_list<RequestCallbackPair_t> requests)
{
	for (auto& req: requests)
	{
		req_map.push_back(req);
	}
}

void Server::add_route(RequestCallbackPair_t req)
{
	req_map.emplace_back(std::move(req));
}

std::optional<HTTP::Response const> Server::handle_request(HTTP::Request &request)
{
	for (auto& reqs: req_map)
	{
		auto args = reqs.first.match_get_args(request);
		if (args)
		{
			return reqs.second(cb_args, request, *args);
		}
	}
	return std::nullopt;
}

void Server::log_request(const HTTP::Request& request)
{
	Logger::log(
		this->logger_cat,
		Logger::Level::INFO,
		std::string(request.get_type_string()) + " " + request.get_url());
}
