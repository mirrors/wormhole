/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include "error_response.h"
#include "jsonhelper.h"
#include "mime.h"

HTTP::Response
HTTP::Error::make_json_error_response(const std::string& error, HTTP::Code code)
{
	rjson::Document root(rjson::kObjectType);
	root.AddMember("error", error, root.GetAllocator());
	return HTTP::Response{ rjson::to_string(root), HTTP::MIME::JSON, code};
}
