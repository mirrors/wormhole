/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include <any>
#include <initializer_list>
#include <cstdint>
#include <unordered_map>
#include <utility>
#include <string_view>
#include "request.h"
#include "logger.h"
#include "response.h"

namespace HTTP
{
	// The `status' could be extra helpful for debugging
	static std::string json_404 = "{\"status\":404,\"error\":\"Unknown resource\"}\n";

	class Server
	{
	public:
		explicit Server(uint16_t port, std::any callback_args);
		virtual ~Server() = default;

		/**
		 * @brief Starts a server.
		 *
		 * If the server is already started, nothing will occur.
		 */
		virtual void start();

		/**
		 * @brief Stops a server.
		 *
		 * If the server is stopped, nothing will occur.
		 */
		virtual void stop();

		/**
		 * @brief Maps a list of routes for the server to work with.
		 *
		 * @see HTTP::Request
		 *
		 * These are mainly URLs and the request type that the HTTP server will
		 * map to a function
		 */
		void add_routes(std::initializer_list<RequestCallbackPair_t> requests);
		void add_route(RequestCallbackPair_t request);

		/**
		 * @brief Handles a crafted HTTP Request
		 *
		 * Calls a function if the url matches, or it calls the custom error provided
		 *
		 * @param request The request to check
		 * @return An HTTP response which contains the information
		 */
		std::optional<HTTP::Response const> handle_request(HTTP::Request& request);

		/**
		 * @brief Logs a request to the Logger instance
		 *
		 * Mainly for debugging and seeing if things are working
		 */
		void log_request(const HTTP::Request& request);

		inline uint16_t get_port() const noexcept { return port; }
	protected:
		std::vector<RequestCallbackPair_t> req_map;
		Logger::category_id logger_cat;

	private:
		std::any cb_args;
		/** Port number, should be set once */
		uint16_t port;
	};
}
