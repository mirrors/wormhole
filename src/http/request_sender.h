/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once
#include <memory>
#include <curl/curl.h>
#include <string_view>

// Hey, remember when we used this for sending to clients with LibMicrohttpd?
#include "request.h"

namespace HTTP
{
	/**
	 * @brief A request sender wrapping Curl Multi
	 */
	class RequestSender
	{
	public:
		RequestSender();
		~RequestSender() = default;

		/**
		 * @brief Sends a HTTP request to a server.
		 *
		 * @remark We're actually reusing the Requests that were
		 *  originally intended for send back to clients. Not really
		 *  intended for Curl originally, but oh well.
		 */
		void request(const HTTP::Request& request) const;
	private:
		std::unique_ptr<CURLM, decltype(&curl_multi_cleanup)> curl;
	};
}
