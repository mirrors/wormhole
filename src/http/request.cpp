/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include <algorithm>
#include "request.h"

using namespace HTTP;

Request::Request(Type type, const std::string url, unsigned mimetypes /* = HTTP::MIME::ANY */)
	: type{type},
	  url{std::move(url)},
	  mimetypes{mimetypes},
	  param{}
{}

bool Request::operator==(const Request& other) const
{
	return get_url() == other.get_url() && get_type() == other.get_type();
}

std::optional<RequestArgs_t> Request::match_get_args(Request& other)
{
	other.try_strip_slash();
	
	// Before doing anything, check if the type (POST, GET...) are the same
	if (other.get_type() != this->type) return std::nullopt;
	// And the Mimetype, where `other' can have one mime and `this' is a Bitwise OR
	if ((this->mimetypes & other.get_mime()) == 0) return std::nullopt;
	
	std::vector<std::string> args{};
	// Current point of other->url
	size_t curr = 0;
	// Current point of this->url
	size_t n = 0, n_last = 0;

	const std::string& other_url = other.get_url();

	// Check if path is / and request is ""
	// Args is empty, but still returned...
	if (url == "/" && other.url == "") return args;

	while((n = url.find(':', n+1)) != std::string::npos)
	{
		// Compare range from start to found point
		if (!std::equal(url.begin() + n_last,
			url.begin() + n_last + (n-1),
			other_url.begin() + curr))
			break;

		// Peek next character to know where to stop (often a '/')
		curr += n - n_last;
		size_t next_char_dist = other_url.find(url[n+1], curr);
		if (next_char_dist != std::string::npos)
		{
			// Push string at index
			args.push_back(other_url.substr(n, next_char_dist - n));
			// Seek n past next_char
			curr += next_char_dist - n;
		}
		else {
			const std::string tmp = other_url.substr(n);
			// If this string has a / in it, this it's not _quite_ it (but close)
			if (tmp.find('/') != std::string::npos)
	return std::nullopt;
			// Argument is at end of string
			args.emplace_back(tmp);
		}
		
		n_last = n;
	}

	// If previous loop couldn't find anything, check if strings aren't fully equal
	// Equivalent behavior in Request::operator==
	if (n == std::string::npos &&
		n_last == 0 &&
		url == other_url)
	{
		// ... if so, this matches directory!
		return args;
	}

	// Compare rest of n
	if (url.length() != n_last && url[n_last + 1] != '\0')
	{
		if (!std::equal(url.begin() + n_last + 1,
			url.begin() + url.length() - 1,
			other_url.begin() + curr))
			return std::nullopt;
	}

	return args;
}

bool Request::try_strip_slash()
{
	if (url.length() == 0 || url == "/") return false;
	bool ret = false;
	// While loop because the user can just pass /url///////////...
	while (url.back() == '/' && url.length() != 1)
	{
		url.pop_back();
		ret = true;
	}
	return ret;
}

std::string_view Request::get_type_string() const noexcept
{
	switch (type)
	{
	case Request::Type::GET: return "GET";
	case Request::Type::POST: return "POST";
	case Request::Type::PUT: return "PUT";
	case Request::Type::DELETE: return "DELETE";
	case Request::Type::PATCH: return "PATCH";
	case Request::Type::OPTIONS: return "OPTIONS";
	default:
		return "...OTHER";
	}
}
