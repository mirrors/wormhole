/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include <string>
#include "responsecode.h"
#include "response.h"

namespace HTTP
{
	namespace Error
	{
		HTTP::Response make_json_error_response(const std::string& error, HTTP::Code code = HTTP::Code::BAD_REQUEST);
	}
	
}
