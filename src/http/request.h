/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include <any>
#include <functional>
#include <string>
#include <string_view>
#include <utility>
#include <optional>
#include <vector>
#include <unordered_map>
#include "response.h"
#include "mime.h"

namespace HTTP
{
	class Request;
	/*
	 * These aren't used internally by the request class,
	 * but they are useful for the HTTP Server and other things as well
	 */
	using RequestArgs_t = std::vector<std::string>;
	using RequestCallback_t = std::function<
		std::optional<HTTP::Response const>( std::any&, Request&, RequestArgs_t& )>;
	using RequestCallbackPair_t = std::pair<Request, RequestCallback_t>;

	using KeyStrVarMap = std::map<std::string,
		  StringVariant>;

	/**
	 * @brief A HTTP Request structure
	 *
	 * A very simple format is used for the URL to match paremeters.
	 * If a ':' character is in place, then it will read until the next character (often '/')
	 * or until the next end of line. (rules don't apply for POD Request instance)
	 *
	 * Example: /api/v1/user/:/haters
	 *
	 * Example 2: /api/v1/user/:/dislike_user/:
	 *
	 * Example 3: /lets/get/:_fails/or/wins_:
	 */
	class Request
	{
	public:
		enum class Type {
			GET,
			POST,
			PUT,
			DELETE,
			PATCH,
			OPTIONS,
			OTHER,
		};

		/**
		 * @brief Request constructor
		 *
		 * General usage is to compare against another request (with a function),
		 *  with call_if_match
		 *
		 * @param type Type of request, @see Request::Type
		 * @param req Request match parameter
		 * @param mimetypes Bitwise OR set of Mimetypes
		 * 
		 * @example Request(Request::GET, "/api/v1/user/:", MIME::ACTIVITY_JSON | MIME::XML)
		 */
		Request(Type type, std::string req, unsigned mimetypes = HTTP::MIME::ANY);
		~Request() = default;

		/**
		 * Compares type and url as is
		 *
		 * @remark Does *not* expand ':', useful if you need to do a
		 *  simple comparison, and if it's false, then use Request::match_get_args
		 *
		 * i.e. POST "/api/v1/instance" == POST "/api/v1/instance"
		 *
		 * @param other To compare against
		 */
		bool operator==(const Request& other) const;
		inline bool operator!=(const Request& other) const { return !operator==(other); }

		/**
		 * Compares a request with another requests, and expands ':' to arguments
		 *  when comparing. If no expansion required, equivalent to Request::operator==, but returns an empty std::vector
		 *
		 * @param request Request to compare (and expand against)
		 * @return Vector of strings to arguments, may be empty if no expansions
		 * @return std::nullopt if the match fails entirely
		 */
		std::optional<RequestArgs_t> match_get_args(Request& request);

		/**
		 * @brief Strips ending slash
		 * @remark Strips multiple slashes if any
		 * @return True unless any slashes were stripped
		 */
		bool try_strip_slash();

		// Type shouldn't be modifyable
		inline Type get_type() const noexcept { return type; }
		std::string_view get_type_string() const noexcept;
		/** @remark May be a path */
		inline const std::string& get_url() const noexcept { return url; }
		/** @remark Assuming this wasn't a propagated mimetype (OR mime) */
		inline HTTP::MIME::Mimetypes get_mime() const noexcept
		{ return static_cast<HTTP::MIME::Mimetypes>(mimetypes); }

		/** @brief GET or POST parameters */
		KeyStrVarMap param;

		/** @see mime.h */
		unsigned mimetypes;

		std::string_view data;
		
	private:
		Type type;
		/** @remark Used as a plain old path too. */
		std::string url;
	};
}
