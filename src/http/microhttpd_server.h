/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once
#include <cstdint>
#include <memory>
#include <microhttpd.h>
#include "httpserver.h"

namespace HTTP
{
	class MicroHttpdServer : public Server
	{
	public:
		explicit MicroHttpdServer(std::uint16_t port, std::any callback_args);
		virtual ~MicroHttpdServer() = default;

		virtual void start() override final;
		virtual void stop() override final;
	private:
		struct MHDDaemonDeleter { void operator()(struct MHD_Daemon* d) { MHD_stop_daemon(d); } };
		std::unique_ptr<MHD_Daemon, MHDDaemonDeleter> serv;
	};
}
