/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include <cstdint>
#include <string>
#include <string_view>

namespace HTTP
{
	namespace MIME
	{
		enum Mimetypes : uint8_t
		{
			HTML = 1<<0,
			CSS = 1<<1,
			JSON = 1<<2,
			ACTIVITY_JSON = 1<<3,
			LD_JSON = 1<<4,
			XML = 1<<5,
			OTHER = 1<<6,
			ANY = 0xFF,
		};

		constexpr uint8_t MIME_JSON_TYPE = JSON | ACTIVITY_JSON | LD_JSON;

		using mime_t = enum Mimetypes;
		using mime_str_t = std::string_view;
		
		extern std::string_view HTML_STR; // = "text/html"
		extern std::string_view CSS_STR; // = "text/css"
		extern std::string_view JSON_STR; // = "application/json"
		extern std::string_view ACTIVITY_JSON_STR; // = "application/activity+json"
		extern std::string_view LD_JSON_STR; // = "application/ld+json"
		extern std::string_view XML_STR; // = "application/xml"

		/**
		 * @brief Converts Mime string to Mime enumeration type
		 * @example "text/html" returns HTTP::MIME::HTML
		 * @param mime Mime string
		 * @return Converted Mime enum type
		 */
		MIME::mime_t str_to_mime(MIME::mime_str_t mime);

		/**
		 * @brief Converts Mime enumeration type to Mime string
		 * @example HTTP::MIME::JSON returns "application/json"
		 * @param mime Mime enumeration type
		 * @return Converted Mime string
		 */
		MIME::mime_str_t mime_to_str(MIME::mime_t mime);
	}
}
