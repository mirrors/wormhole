/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include <cstdint>
#include <cstring>
#include <iostream>
#include <utility>
#include <microhttpd.h>
#include <cerrno>
#include "config/config_loader.h"
#include "logger.h"
#include "microhttpd_server.h"
#include "http/mime.h"
#include "http/request.h"
#include "http/response.h"

using namespace std::string_literals;
using namespace HTTP;

constexpr int POSTBUFFERSIZE = 2048;

// Struct we define to hold onto data
struct MHDConnectionState
{
	std::string data;
};

namespace
{
#ifndef NDEBUG
	void
	debug_cross_origin(MHD_Response* response)
	{
		// Useful for testing with frontends
		MHD_add_response_header(response, "Access-Control-Allow-Origin", "*");
	}
#endif // NDEBUG
	
	HTTP::Request::Type
	str_to_method(std::string method)
	{
		// i.e. if (method == "GET") return HTTP::Request::Type::GET;
#define IF_METHOD(val) if (method == #val) return HTTP::Request::Type::val
		IF_METHOD(GET);
		IF_METHOD(POST);
		IF_METHOD(PUT);
		IF_METHOD(DELETE);
		IF_METHOD(PATCH);
		IF_METHOD(OPTIONS);
		else return HTTP::Request::Type::OTHER;
	}

	// Iterator function for POST, GET, etc.
	// 
	enum MHD_Result
	gcv_iterator(void* cls, MHD_ValueKind kind,
	 const char* key, size_t key_len,
	 const char* value, size_t value_len)
	{
		HTTP::Request& request = *reinterpret_cast<HTTP::Request*>(cls);

		switch (kind)
		{
		case MHD_GET_ARGUMENT_KIND:
			request.param.insert({ {key, key + key_len}, std::string(value, value_len)});
			break;
		case MHD_HEADER_KIND:
			if (std::string_view(key, key_len) == "Content-Type")
			{
	request.mimetypes = MIME::str_to_mime({value, value_len});
			}
	
			break;
		default:
			// Unhandled
			break;
		}
		
		return MHD_YES;
	}


	void
	request_completed(void* cls,
		  struct MHD_Connection* connection,
		  void** con_cls,
		  enum MHD_RequestTerminationCode toe)
	{
		MHDConnectionState* con_info = reinterpret_cast<MHDConnectionState*>(*con_cls);

		// Nothing to clear here
		if (!con_info)
			return;

		delete con_info;
	}

	// See libmicrohttpd docs, this function will call itself recursively
	enum MHD_Result
	new_connection(void* cls,
	   MHD_Connection* conn,
	   const char* url,
	   const char* _method,
	   const char* version,
	   const char* post_data,
	   size_t* post_data_size,
	   void** con_cls)
	{
		MicroHttpdServer* that = reinterpret_cast<MicroHttpdServer*>(cls);
		MHD_Response* response;
		HTTP::Request::Type method = str_to_method(_method);
		enum MHD_Result ret;
		MHDConnectionState* state = reinterpret_cast<MHDConnectionState*>(*con_cls);

		if (state == nullptr)
		{
			*con_cls = new MHDConnectionState{};
			return MHD_YES;
		}
		else if (*post_data_size) {
			state->data += std::string(post_data, post_data + *post_data_size);
			*post_data_size = 0;
			return MHD_YES;
		}

		// Create generic request from args
		HTTP::Request req{method, url};

		if (!state->data.empty())
			req.data = state->data;

		MHD_get_connection_values_n(conn,
			static_cast<MHD_ValueKind>(
				MHD_GET_ARGUMENT_KIND | MHD_HEADER_KIND),
			gcv_iterator,
			&req);

		that->log_request(req);
		
		// Call response, ensure that the request above is proprogated well!
		std::optional<HTTP::Response> resp = that->handle_request(req);

		if (!resp)
		{
			// Return a 404
			response = MHD_create_response_from_buffer(json_404.length(),
				(void*)(json_404.data()),
				MHD_RESPMEM_PERSISTENT);
#ifndef NDEBUG
			debug_cross_origin(response);
#endif // NDEBUG
			MHD_add_response_header(response, "Content-Type", MIME::JSON_STR.data());
			ret = MHD_queue_response(conn, MHD_HTTP_NOT_FOUND, response);
			MHD_destroy_response(response);
			return ret;
		}
		// TODO URGENT Slow to copy each time... Do alternative here.
		response = MHD_create_response_from_buffer(resp->data.size(),
			   (void*)(resp->data.data()),
			   MHD_RESPMEM_MUST_COPY);

		MHD_add_response_header(response, "Content-Type", MIME::mime_to_str(resp->mimetype).data());

		// Useful for testing with frontends
#ifndef NDEBUG
		debug_cross_origin(response);
#endif // NDEBUG
		
		// HTTP::Code works for MHD already since it's just int's that represent the HTTP response codes
		ret = MHD_queue_response(conn, resp->code, response);
		
		MHD_destroy_response(response);
		
		return ret;
	}
}

MicroHttpdServer::MicroHttpdServer(std::uint16_t port, std::any callback_args)
	: Server(port, callback_args),
	  serv(nullptr)
{}

void MicroHttpdServer::start()
{
	MHD_Daemon* dm = MHD_start_daemon(MHD_USE_AUTO | MHD_USE_INTERNAL_POLLING_THREAD,
			  get_port(),
			  nullptr,
			  0,
			  &::new_connection,
			  this,
			  MHD_OPTION_NOTIFY_CONNECTION,
			  request_completed,
			  this,
			  MHD_OPTION_THREAD_POOL_SIZE,
			  Config::instance().config.http.pool_size,
			  MHD_OPTION_END);
	if (!dm)
	{
		// MicroHTTPD seems to keep the errors in errno, but doesn't really have a good error system
		Logger::log(logger_cat,
		Logger::Level::ERROR,
		"Couldn't start MicroHTTPD daemon at port "s + std::to_string(get_port()) +
		": " + std::strerror(errno));
		exit(1);
	}
	
	serv.reset(dm);
}



void MicroHttpdServer::stop()
{
	// Will call "destructor"
	serv = nullptr;
}

