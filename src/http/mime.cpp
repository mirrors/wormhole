/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include "http/mime.h"

std::string_view HTTP::MIME::HTML_STR = "text/html";
std::string_view HTTP::MIME::CSS_STR = "text/css";
std::string_view HTTP::MIME::JSON_STR = "application/json";
std::string_view HTTP::MIME::ACTIVITY_JSON_STR = "application/activity+json";
std::string_view HTTP::MIME::LD_JSON_STR = "application/ld+json";
std::string_view HTTP::MIME::XML_STR = "application/xml";

HTTP::MIME::mime_t HTTP::MIME::str_to_mime(HTTP::MIME::mime_str_t mime)
{
	// i.e. if (mime == HTTP::MIME::MIME_HTML_STR) return HTTP::MIME::MIME_HTML;
#define IF_MIME(val) if (mime == HTTP::MIME::val ## _STR) return HTTP::MIME::val
	IF_MIME(HTML);
	IF_MIME(CSS);
	IF_MIME(JSON);
	IF_MIME(ACTIVITY_JSON);
	IF_MIME(LD_JSON);
	IF_MIME(XML);
	else
		return HTTP::MIME::OTHER;
}

// Opposite of the last function
HTTP::MIME::mime_str_t HTTP::MIME::mime_to_str(HTTP::MIME::mime_t mime)
{
	// i.e. if (mime == HTTP::MIME::MIME_HTML) return HTTP::MIME::MIME_HTML_STR;
#define IF_STR(val) if (mime == HTTP::MIME::val) return HTTP::MIME::val ## _STR
	IF_STR(HTML);
	IF_STR(CSS);
	IF_STR(JSON);
	IF_STR(ACTIVITY_JSON);
	IF_STR(LD_JSON);
	IF_STR(XML);
	else
		return HTTP::MIME::JSON_STR;
}
