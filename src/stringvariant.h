/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once
#include <utility>
#include <string>

/**
 * @brief Little helper class which wraps around a string
 *
 * Mainly used for the GET/POST value maps
 */
class StringVariant
{
public:
	StringVariant(const std::string str) : str{std::move(str)} {}
	~StringVariant() = default;

	inline unsigned long as_unsigned_long() const { return std::stoul(str); }
	inline long as_long() const { return std::stol(str); }
	inline int as_int() const { return std::stoi(str); }
	inline float as_float() const { return std::stof(str); }
	inline double as_double() const { return std::stof(str); }
	inline const std::string& string() const { return str; }
private:
	const std::string str;
};
