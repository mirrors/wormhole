/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include <cstdlib>
#include "database.h"
#include "sqlite/sqlite.h"
#include "config/config_db.h"
#include "config/config_loader.h"
#include "logger.h"

std::shared_ptr<DB::Database> DB::load_db_from_cfg()
{
	Logger::category_id log_id =
		Logger::make_category("Database config");
	std::shared_ptr<DB::Database> database = nullptr;

	// Select Database
	switch (Config::instance().config.database.type)
	{
	case DB::Database::Type::Unset:
		Logger::log(log_id,
		Logger::Level::WARN,
		"Database not set in config.yaml, using SQLite3!");
		// Fall
		
	case DB::Database::Type::SQLite:
		database = std::make_shared<DB::SQLite>(
			Config::instance().config.database.file);
		break;
		
	case DB::Database::Type::PostgreSQL:
		Logger::log(log_id,
		Logger::Level::WARN,
		"Not implemented!");
		return nullptr;
		
	default:
		Logger::log(log_id,
		Logger::Level::WARN,
		"Unsupported Database!");
		return nullptr;
	}

	return database;
}
