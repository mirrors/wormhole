/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include <string>
#include <memory>
#include "types/user.h"
#include "types/app.h"
#include "crypt/rsa.h"
#include "random.h"
#include "types/config.h"

namespace DB
{
	class Database
	{
	public:
		explicit Database() : rng{}, rsa_cstor{}
		{}
		
		virtual ~Database() = default;

		/**
		 * @brief Attempts to migrate the database
		 * @
		 * @return True unless no migration was done
		 */
		virtual bool try_migration() {return false;};

		// Users
		virtual User get_user(decltype(User::id) id) {return {};}
		virtual User get_user(const decltype(User::acct)& acct) {return {};}
		virtual void create_user(User& props) {}
		virtual void delete_user(decltype(User::id) id) {}
		virtual void delete_user(const decltype(User::acct)& acct) {}

		// Applications/OAuth
		virtual App get_app(decltype(App::id) id) {return {};}
		virtual App get_app_by_client_id(const decltype(App::client_id)& client_id)
		{return {};}
		virtual App get_app_by_client_secret(const decltype(App::client_id)& client_secret)
		{return {};}
		virtual void create_app(App& props) {}
		virtual void delete_app(decltype(App::id) id) {}
		virtual void delete_app_by_client_id(const decltype(App::client_id)& id) {}
		virtual void delete_app_by_client_secret(const decltype(App::client_id)& id) {}
		/// There is only one config!
		virtual Config_t get_config()
		{return {};}

		/// These are just enums for Configuration and stuff, but Polymorphism
		/// handles the typing stuff already
		enum class Type
		{
			SQLite,
			PostgreSQL,
			Unset,
		};
	protected:
		Random rng;
		Crypt::RsaFactory rsa_cstor;
	};

	std::shared_ptr<DB::Database> load_db_from_cfg();
}
