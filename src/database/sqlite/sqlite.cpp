/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include <string_view>
#include <stdexcept>
#include <utility>
#include "sqlite.h"
#include <variant>
#include <type_traits>
#include <cassert>
#include <iostream>
#include <initializer_list>

using namespace DB;
using namespace std::string_literals;

/** Informational struct for @function propagate */
struct ppgate_sqlite_sel_info
{
	std::variant<int*, unsigned*, bool*, std::string*> type;
	bool maybe_null;
};

static void
propagate_sqlite_selection(sqlite3_stmt* stmt,
		 // kys
		 const
		 std::initializer_list<ppgate_sqlite_sel_info> info)
{
	using namespace std;
	
	for (size_t i = 0; i < info.size(); ++i)
	{
		auto it = info.begin() + i;
		
		if (it->maybe_null &&
			sqlite3_column_type(stmt, i) == SQLITE_NULL)
			continue;

		std::visit([stmt, i](auto&& arg) {
			using T = remove_cv_t<remove_reference_t<decltype(arg)>>;
			if constexpr (is_same_v<T, int*> ||
		is_same_v<T, unsigned*> ||
		is_same_v<T, bool*>)
	*arg = ::sqlite3_column_int(stmt, i);
			else if constexpr (is_same_v<T, std::string*>)
	*arg = reinterpret_cast<char const*>
		(::sqlite3_column_text(stmt, i));
		}, it->type);
	}
}

SQLite::SQLite(std::filesystem::path path)
	: DB::Database(),
	  path{ std::move(path) },
	  db{ nullptr },
	  sql_cache(),
	  logger_cat{ Logger::make_category("SQLite") }
{
	if (sqlite3_open(this->path.c_str(), &db))
	{
		const std::string err_msg = "Can't open database: "s +
			std::string( sqlite3_errmsg(db) );
		sqlite3_close( db );
		throw std::runtime_error( err_msg );
	}

	Logger::log(logger_cat,
	Logger::Level::DEBUG,
	"SQLite3 database opened at "s + this->path.string());
}

SQLite::~SQLite()
{
	// Close 'cached' statements
	for (sqlite3_stmt* s: sql_cache)
	{
		sqlite3_finalize(s);
	}
	sqlite3_close(db);
}

int SQLite::sqlite_exec(SQLiteCacheIndex idx,
			std::initializer_list<SQLiteBindType_t> keys,
			std::function<void(sqlite3_stmt*)> func)
{
	int code;
	sqlite3_stmt* stmt = sql_cache[idx];
	assert(stmt != nullptr);

	Logger::log(logger_cat,
	Logger::Level::DEBUG,
	"Executing query: "s + sqlite3_sql(stmt));

	int query_count = 0;

	int key_i = 1;
	for (auto&& key: keys)
	{
		std::visit([&code, stmt, key_i](auto&& arg) {
			using T = std::remove_cv_t<std::remove_reference_t<decltype(arg)>>;
			if constexpr (std::is_same_v<T, int>)
	code = sqlite3_bind_int(stmt, key_i, arg);
			else if constexpr (std::is_same_v<T, unsigned long>)
	code = sqlite3_bind_int64(stmt, key_i, static_cast<unsigned long>(arg));
			else if constexpr (std::is_same_v<T, double>)
	code = sqlite3_bind_double(stmt, key_i, arg);
			else if constexpr (std::is_same_v<T, std::string>)
	code = sqlite3_bind_text(stmt, key_i, arg.c_str(), arg.length(), SQLITE_STATIC);
		}, key);

		if (code == SQLITE_ERROR)
		{
			const std::string err_msg = "Couldn't bind argument: "s + std::string(sqlite3_errmsg(db));
			sqlite3_reset(stmt);
			throw std::runtime_error(err_msg);
		}
		
		++key_i;
	}

	while ((code = sqlite3_step(stmt)) == SQLITE_ROW)
	{
		if (func) func(stmt);
		++query_count;
	}

	switch (code)
	{
	case SQLITE_BUSY:
		Logger::log(logger_cat,
		Logger::Level::DEBUG,
		"SQLite database is busy!");
		break;
	case SQLITE_ERROR:
	default:
	case SQLITE_MISUSE:
	{
		const std::string err_msg = "Couldn't exec query: "s + std::string(sqlite3_errmsg(db));
		sqlite3_reset(stmt);
		throw std::runtime_error(err_msg);
	}
	case SQLITE_DONE:
		break;
	}
	
	sqlite3_reset(stmt);
	return query_count;
}

SQLiteCacheIndex SQLite::sqlite_compile_to_cache(SQLiteCacheIndex idx, std::string_view sql)
{
	int rc;
	if (!sql_cache[idx])
	{
		rc = sqlite3_prepare_v2(db, &*sql.begin(), sql.length(), &sql_cache[idx], nullptr);
		std::cout << sql << std::endl;
		if (rc == SQLITE_ERROR)
		{
			const std::string err_msg = "Couldn't prepare SQL statement: "s + std::string(sqlite3_errmsg(db));
			throw std::runtime_error(err_msg);
		}
	}
	return idx;
}

User SQLite::get_user(decltype(User::id) id)
{
	User luser{};
	luser.id = id;
	luser.local = false;

	// Setup function
	sqlite_compile_to_cache(SQLiteCacheIndex::GET_USER_BY_ID,
				"SELECT acct, display_name, bio, key"
				" FROM users WHERE id=?1");

	int code = sqlite_exec(SQLiteCacheIndex::GET_USER_BY_ID, {{id}}, [&luser](sqlite3_stmt* stmt){
		propagate_sqlite_selection(stmt, {
	{&luser.acct, false},
	{&luser.display_name, true},
	{&luser.bio, true},
	{&luser.key, true},
			});
	});
	
	if (!code)
	{
		throw std::runtime_error("User id \""s + std::to_string(id) + "\" doesn't exist");
	}
	
	return luser;
}

void SQLite::create_user(User& p)
{
	// INSERT INTO users (local, display_name, acct, bio, created_at) VALUES (TRUE, 'Test user', 'test1', 'sneed!!!', unixepoch());
	
	sqlite_compile_to_cache(SQLiteCacheIndex::CREATE_USER,
				"INSERT INTO users ( "
				"local, email, display_name, acct, bio, created_at, updated_at, birthday, key"
				") VALUES ( ?1, ?2, ?3, ?4, ?5, unixepoch(), unixepoch(), ?6, ?7 )");

	p.generate_key(rsa_cstor);

	sqlite_exec(SQLiteCacheIndex::CREATE_USER,
	{p.local, p.email, p.display_name, p.acct,
	 p.bio, p.birthday, p.key},
	nullptr);
}

void SQLite::delete_user(decltype(User::id) id)
{
	// Setup function
	sqlite_compile_to_cache(SQLiteCacheIndex::DELETE_USER_BY_ID,
				"DELETE FROM users WHERE id=?1");

	int code = sqlite_exec(SQLiteCacheIndex::DELETE_USER_BY_ID, {{id}}, nullptr);
	
	if (!code)
	{
		throw std::runtime_error("User id \""s + std::to_string(id) + "\" doesn't exist");
	}
}

void SQLite::delete_user(const decltype(User::acct)& acct)
{
	// Setup function
	sqlite_compile_to_cache(SQLiteCacheIndex::DELETE_USER_BY_ID,
				"DELETE FROM users WHERE acct=?1");

	int code = sqlite_exec(SQLiteCacheIndex::DELETE_USER_BY_ID, {{acct}}, nullptr);
	
	if (!code)
	{
		throw std::runtime_error("Account \""s + acct + "\" doesn't exist");
	}
}

App SQLite::get_app(decltype(App::id) id)
{
	App app{};
	app.id = id;

	// Setup function
	sqlite_compile_to_cache(SQLiteCacheIndex::GET_APP_BY_ID,
				"SELECT client_name, client_id, client_secret,"
				"app_website FROM users WHERE id=?1");

	int code = sqlite_exec(SQLiteCacheIndex::GET_APP_BY_ID, {{id}}, [&app](sqlite3_stmt* stmt){
		propagate_sqlite_selection(stmt, {
	{&app.client_name, true},
	{&app.client_id, true},
	{&app.client_secret, true},
	{&app.app_website, true},
			});
	});
	
	if (!code)
	{
		throw std::runtime_error("App \""s + std::to_string(id) + "\" couldn't be created");
	}
	
	return app;
}

App SQLite::get_app_by_client_id(const decltype(App::client_id)& client_id)
{
	App app{};
	// Very much assumed
	app.client_id = client_id;

	// Setup function
	sqlite_compile_to_cache(SQLiteCacheIndex::GET_APP_BY_ID,
				"SELECT id, client_name, client_secret,"
				"app_website FROM applications WHERE client_id=?1");

	int code = sqlite_exec(SQLiteCacheIndex::GET_APP_BY_ID, {{client_id}}, [&app](sqlite3_stmt* stmt){
		propagate_sqlite_selection(stmt, {
	{&app.id, true},
	{&app.client_secret, true},
	{&app.app_website, true},
			});
	});
	
	if (!code)
	{
		throw std::runtime_error("App \""s + client_id + "\" wasn't found");
	}
	
	return app;
}

App SQLite::get_app_by_client_secret(const decltype(App::client_id)& client_secret)
{
}

void SQLite::create_app(App& p)
{
	sqlite_compile_to_cache(SQLiteCacheIndex::CREATE_APP,
				"INSERT INTO applications ("
				"client_name, client_id, client_secret, app_website, created_at, updated_at"
				")VALUES ( ?1, ?2, ?3, ?4, unixepoch(), unixepoch() )");

	p.generate_tokens(rng);

	sqlite_exec(SQLiteCacheIndex::CREATE_APP,
	{p.client_name, p.client_id, p.client_secret, p.app_website},
	nullptr);
}

void SQLite::delete_app(decltype(App::id) id)
{
}

void SQLite::delete_app_by_client_id(const decltype(App::client_id)& id)
{
}

void SQLite::delete_app_by_client_secret(const decltype(App::client_id)& id)
{
}

User
SQLite::get_user(const decltype(User::acct)& acct)
{
	User luser{};
	luser.id = 0;
	luser.acct = acct;
	luser.local = false;
	
	// Setup function
	sqlite_compile_to_cache(SQLiteCacheIndex::GET_USER_BY_ACCT,
				"SELECT id, display_name, bio, key FROM users WHERE acct=?1");

	int code = sqlite_exec(SQLiteCacheIndex::GET_USER_BY_ACCT,
		 {acct},
		 [&luser](sqlite3_stmt* stmt) {
		propagate_sqlite_selection(stmt, {
	{&luser.id, false},
	{&luser.display_name, true},
	{&luser.bio, true},
	{&luser.key, true},
			});
	});

	if (!code)
	{
		throw std::runtime_error("User \""s + acct + "\" doesn't exist");
	}
	
	return luser;
}

// Retrieves the server configuration that's stored in the database
Config_t
SQLite::get_config()
{
	Config_t config{};

	sqlite_compile_to_cache(SQLiteCacheIndex::GET_CONFIG,
				"SELECT instance_domain, "
				"instance_name, "
				"instance_description, "
				"instance_logo, "
				"instance_admin_email, "
				"instance_background_image, "
				"instance_thumbnail, "
				//"instance_languages, "
				"account_approval_required, "
				"account_registrations_enabled "
				"FROM config WHERE id=0");

	int code = sqlite_exec(SQLiteCacheIndex::GET_CONFIG,
		 {},
		 [&config](sqlite3_stmt* stmt) {
		  propagate_sqlite_selection(stmt, {
			{&config.instance.domain, true},
			{&config.instance.name, true},
			{&config.instance.description, true},
			{&config.instance.logo, true},
			{&config.instance.admin_email, true},
			{&config.instance.background_image, true},
			{&config.instance.thumbnail, true},
			{&config.account.approval_required, true},
			{&config.account.registrations_enabled, true},
			{&config.upload_limit.avatar, true},
			{&config.upload_limit.media, true},
		   });
		 });

	if (!code)
		throw std::runtime_error("Config doesn't exist or improperly setup! Does the table `config' exist?");

	return config;
}


// Note: Please refer to SQLite docs on types, many of these are aliases to others, such as BOOLEAN.
// It's still recommended to use them for clarity
bool SQLite::try_migration()
{
	const char *create_tables[] = {
	  R"sql(
CREATE TABLE IF NOT EXISTS instances (
	id INTEGER PRIMARY KEY ASC,
	host TEXT,
	users_count INTEGER
)
)sql",

	  R"sql(
CREATE TABLE IF NOT EXISTS users (
	id INTEGER PRIMARY KEY ASC,
	email TEXT,
	local BOOLEAN,
	ap_id TEXT,
	ap_follower_addr TEXT,
	ap_following_addr TEXT,
	password_hash TEXT,
	acct TEXT NOT NULL,
	display_name TEXT,
	bio TEXT,
	created_at DATETIME NOT NULL,
	updated_at DATETIME,
	avatar TEXT,
	key TEXT,
	banner TEXT,
	background TEXT,
	statuses_count INTEGER DEFAULT 0,
	follower_count INTEGER DEFAULT 0,
	following_count INTEGER DEFAULT 0,
	last_status DATETIME,
	is_suggested BOOLEAN DEFAULT FALSE,
	is_approved BOOLEAN DEFAULT FALSE,
	birthday DATETIME,
	show_birthday BOOLEAN DEFAULT FALSE,
	janny_level INTEGER DEFAULT 0,
	follow_move BOOLEAN DEFAULT FALSE,
	instance_id INTEGER,
	unique (acct),
	FOREIGN KEY(instance_id) REFERENCES instances(id)
)
)sql",

	  R"sql(
CREATE TABLE IF NOT EXISTS user_activities (
	id INTEGER PRIMARY KEY ASC,
	type INTEGER,
	user INTEGER,
	usee INTEGER,
	activity_finish DATETIME,
	FOREIGN KEY(user) REFERENCES users(id),
	FOREIGN KEY(usee) REFERENCES users(id)
)
)sql",

	  R"sql(
CREATE TABLE IF NOT EXISTS applications (
	id INTEGER PRIMARY KEY ASC,
	client_name TEXT,
	client_id TEXT NOT NULL,
	client_secret TEXT NOT NULL,
	app_website TEXT,
	created_at DATETIME,
	updated_at DATETIME
)
)sql",

	  R"sql(
CREATE TABLE IF NOT EXISTS statuses (
	id INTEGER PRIMARY KEY ASC,
	application TEXT,
	content TEXT,
	language TEXT,
	mentions TEXT /* comma separated array */
)
)sql",

	  /* I was on the fence for if it's better to store databases in something
	* more editor friendly, such as XML, but I figured most users would be
	* happy just dragging their database file.
	*
	* On the `config' schema key: Consider modules. Someone adds a payment
	* system for each post... and they want an easy way to configure it, and
	* they want to _not_ touch Wormhole's source code and give it to other
	* people... strange example indeed, but that's what that's for.
	*/
	  R"sql(
CREATE TABLE IF NOT EXISTS config (
	id INTEGER PRIMARY KEY CHECK (id = 0),
	instance_domain TEXT NOT NULL,
	instance_name TEXT NOT NULL,
	instance_description TEXT,
	instance_logo TEXT, /* media */
	instance_admin_email TEXT,
	instance_background_image TEXT,
	instance_thumbnail TEXT,
	instance_languages TEXT,  /* comma separated "array" */
	instance_rules TEXT, /* json */

	account_approval_required BOOLEAN,
	account_registrations_enabled BOOLEAN,

	upload_limit_avatar INTEGER,  /* bytes... */
	upload_limit_media INTEGER,
	
	max_status_chars INTEGER, /* max_toot_chars :( */
	config BLOB
)
)sql",

//	R"sql(
// CREATE TABLE IF NOT EXISTS statuses (
//	 id INTEGER PRIMARY KEY ASC,
	
// )
// )sql",
	};
	
	char* err;
	for (const char* sql: create_tables)
	{
		if (sqlite3_exec(db, sql, nullptr, nullptr, &err))
		{
			Logger::log(logger_cat,
			Logger::Level::ERROR,
			"Statement: "s + sql);
			
			Logger::log(logger_cat,
			Logger::Level::ERROR,
			err);
			
			throw std::runtime_error(err);
		}
	}
	return false;
}

