/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once
#include <array>
#include <functional>
#include <initializer_list>
#include <utility>
#include <variant>
#include <filesystem>
#include <logger.h>
#include <memory>
#include <sqlite3.h>
#include "../database.h"

namespace DB
{
	using SQLiteBindType_t = std::variant<double,
			int,
			unsigned long,
			std::string>;
	
	enum SQLiteCacheIndex {
		// User
		GET_USER_BY_ID = 0,
		GET_USER_BY_ACCT,
		CREATE_USER,
		DELETE_USER_BY_ID,
		// App
		CREATE_APP,
		GET_APP_BY_ID,
		// Instance / Config
		GET_CONFIG,
		COUNT,
	};
	
	class SQLite : public DB::Database
	{
	public:
		explicit SQLite(std::filesystem::path path);
		virtual ~SQLite();

		virtual bool try_migration() override final;

		// Users
		virtual User get_user(decltype(User::id) id) override final;
		virtual User get_user(const decltype(User::acct)& acct) override final;
		virtual void create_user(User& props) override final;
		virtual void delete_user(decltype(User::id) id) override final;
		virtual void delete_user(const decltype(User::acct)& acct) override final;

		// Applications/OAuth
		virtual App get_app(decltype(App::id) id) override final;
		virtual App get_app_by_client_id(const decltype(App::client_id)& client_id) override final;
		virtual App get_app_by_client_secret(const decltype(App::client_id)& client_secret) override final;
		virtual void create_app(App& props) override final;
		virtual void delete_app(decltype(App::id) id) override final;
		virtual void delete_app_by_client_id(const decltype(App::client_id)& id) override final;
		virtual void delete_app_by_client_secret(const decltype(App::client_id)& id) override final;
		virtual Config_t get_config() override final;
		
	private:
		/**
		 * @brief Executes an SQL statement
		 *
		 * Similar to sqlite3_exec(), but supports the statement caching
		 *
		 * @param idx SQLite cached statement
		 * @param keys List of arguments to bind to the statement
		 * @param func Callback for each row
		 *
		 * @return Number of rows iterated, 0 if none.
		 */
		int sqlite_exec(SQLiteCacheIndex idx,
			std::initializer_list<SQLiteBindType_t> keys,
			std::function<void(sqlite3_stmt*)> func);
		SQLiteCacheIndex sqlite_compile_to_cache(SQLiteCacheIndex idx, std::string_view sql);

		std::array<sqlite3_stmt*, SQLiteCacheIndex::COUNT> sql_cache;
		std::filesystem::path path;
		struct SQLiteDeleter { void operator()(sqlite3* db) { sqlite3_close(db); } };
		sqlite3* db;
		Logger::category_id logger_cat;
	};
}
