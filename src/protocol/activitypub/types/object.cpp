/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */


#include "object.h"

using namespace Protocol;

namespace {
	template <typename T>
	void object(T& root,
	const std::string& type,
	ActivityPub::KeyValues_t pairs,
	rjson::Document::AllocatorType& a)
	{
		root.AddMember("type", std::move(type), a);
		for (const ActivityPub::KeyValuePair_t& kv: std::move(pairs))
		{
			root.AddMember(kv.first, kv.second, a);
		}
	}
}

void ActivityPub::to_object(rjson::Document& root,
				const std::string& type,
				ActivityPub::KeyValues_t pairs,
				rjson::Document::AllocatorType& a)
{
	root.AddMember("@context", "https://www.w3.org/ns/activitystreams", a);
	::object<decltype(root)>(root, type, pairs, a);
}

void ActivityPub::to_object(rjson::Value& root,
				const std::string& type,
				ActivityPub::KeyValues_t pairs,
				rjson::Document::AllocatorType& a)
{
	::object<decltype(root)>(root, type, pairs, a);
}

