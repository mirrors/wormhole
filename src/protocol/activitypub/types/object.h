/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

/// ActivityStream "object" type
#pragma once

#include <string>
#include <utility>
#include <vector>
#include "jsonhelper.h"

#define AP_pair(key, ...) { rjson::Value(key), rjson::Value(__VA_ARGS__) }

namespace Protocol
{
	namespace ActivityPub
	{
		using KeyValuePair_t = std::pair<rjson::Value&&, rjson::Value&&>;
		using KeyValues_t = std::initializer_list<KeyValuePair_t>;

		/**
		 * @brief Propagates a JSON object to create an ActivityStreams root object.
		 *
		 * Object will contain the "@context" value at the base of it.
		 *
		 * @remark Yell at the RapidJSON devs for not making this Polymorphic, it would've been nice
		 *  to do a dynamic_cast on it. There's a template hack to work around this mess.
		 */
		void to_object(rjson::Document& root,
		const std::string& type,
		KeyValues_t pairs,
		rjson::Document::AllocatorType& allocator);

		/**
		 * @brief Propagates a JSON object to create an ActivityStreams object.
		 *
		 * For children of parent objects, this function should be used.
		 */
		void to_object(rjson::Value& root,
		const std::string& type,
		KeyValues_t pairs,
		rjson::Document::AllocatorType& allocator);
	}
}
