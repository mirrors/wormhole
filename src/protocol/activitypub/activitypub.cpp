/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include "instance/instance.h"
#include "jsonhelper.h"
#include <stdexcept>
#include <utility>
#include "route_args.h"
#include "http/mime.h"
#include "http/response.h"
#include "http/responsecode.h"
#include "types/user.h"
#include "activitypub.h"
#include "types/object.h"
#include "http/httpserver.h"
#include "http/request.h"
#include "logger.h"
#include "http/error_response.h"

using namespace Protocol;

HTTP::Response
Route::ActivityPub::user_inbox(std::any& args,
		  const HTTP::Request& req,
		  const HTTP::RequestArgs_t& arg)
{
	using namespace std::string_literals;
	
	DESTRUCT_WORMHOLE_ARGS(args);
	
	rjson::Document root(rjson::kObjectType);
	rjson::Document::AllocatorType& a = root.GetAllocator();
	rjson::Value links(rjson::kArrayType);
	
	// Stored result of account string, stripped from resource
	User user;

	try
	{
		user = db->get_user(arg.at(0));
	}
	catch (...) {
		return HTTP::Error::make_json_error_response("Couldn't find user", HTTP::Code::NOT_FOUND);
	}
	
	const std::string inbox_url = Instance::instance_host_https(true) + "users/" + user.acct + "/inbox";

	Protocol::ActivityPub::
		to_object(root, "OrderedCollection",
	  {
		  AP_pair("id", inbox_url, a),
		  AP_pair("first", inbox_url + "?page=true", a),
		  AP_pair("totalItems", 0),
	  }, a);
		
	return HTTP::Response( rjson::to_string(root), HTTP::MIME::ACTIVITY_JSON );
}

HTTP::Response
Route::ActivityPub::user_outbox(std::any& args,
		const HTTP::Request& req,
		const HTTP::RequestArgs_t& arg)
{
	using namespace std::string_literals;
	
	DESTRUCT_WORMHOLE_ARGS(args);
	
	rjson::Document root(rjson::kObjectType);
	rjson::Document::AllocatorType& a = root.GetAllocator();
	
	// Stored result of account string, stripped from resource
	User user;

	try
	{
		user = db->get_user(arg.at(0));
	}
	catch (...) {
		return HTTP::Error::make_json_error_response("Couldn't find user", HTTP::Code::NOT_FOUND);
	}

	const std::string outbox_url = Instance::instance_host_https(true) + "users/" + user.acct + "/outbox";
	
	Protocol::ActivityPub::
		to_object(root, "OrderedCollection",
	  {
		  AP_pair("id", outbox_url, a),
		  AP_pair("first", outbox_url + "?page=1", a),
		  AP_pair("totalItems", 0),
	  }, a);
	
	return HTTP::Response( rjson::to_string(root), HTTP::MIME::ACTIVITY_JSON );
}

HTTP::Response
Route::ActivityPub::user_followers(std::any& args,
		   const HTTP::Request& req,
		   const HTTP::RequestArgs_t& arg)
{
	using namespace std::string_literals;
	
	DESTRUCT_WORMHOLE_ARGS(args);
	
	rjson::Document root(rjson::kObjectType);
	rjson::Document::AllocatorType& a = root.GetAllocator();
	
	// Stored result of account string, stripped from resource
	User user;

	try
	{
		user = db->get_user(arg.at(0));
	}
	catch (...) {
		return HTTP::Error::make_json_error_response("Couldn't find user", HTTP::Code::NOT_FOUND);
	}
	
	const std::string followers_url = Instance::instance_host_https(true) + "users/" + user.acct + "/followers";

	Protocol::ActivityPub::
		to_object(root, "OrderedCollection",
	  {
		  AP_pair("id", followers_url, a),
		  AP_pair("first", followers_url + "?page=1", a),
		  AP_pair("totalItems", 0),
	  }, a);
	
	return HTTP::Response( rjson::to_string(root), HTTP::MIME::ACTIVITY_JSON );
}

HTTP::Response
Route::ActivityPub::user_following(std::any& args,
		   const HTTP::Request& req,
		   const HTTP::RequestArgs_t& arg)
{
	DESTRUCT_WORMHOLE_ARGS(args);
	using namespace std::string_literals;
	
	rjson::Document root(rjson::kObjectType);
	rjson::Document::AllocatorType& a = root.GetAllocator();
	
	// Stored result of account string, stripped from resource
	User user;

	try
	{
		user = db->get_user(arg.at(0));
	}
	catch (...) {
		return HTTP::Error::make_json_error_response("Couldn't find user", HTTP::Code::NOT_FOUND);
	}

	const std::string following_url = Instance::instance_host_https(true) + "users/" + user.acct + "/following";
	
	Protocol::ActivityPub::
		to_object(root, "OrderedCollection",
	  {
		  AP_pair("id", following_url, a),
		  AP_pair("first", following_url + "?page=1", a),
		  AP_pair("totalItems", 0),
	  }, a);
	
	return HTTP::Response( rjson::to_string(root), HTTP::MIME::ACTIVITY_JSON );
}


HTTP::Response Route::ActivityPub::user(std::any& args, const HTTP::Request& req, const HTTP::RequestArgs_t& arg)
{
	DESTRUCT_WORMHOLE_ARGS(args);
	
	using namespace std::string_literals;
	
	rjson::Document root(rjson::kObjectType);
	rjson::Document::AllocatorType& a = root.GetAllocator();
	// Stored result of account string, stripped from resource
	User user;

	try
	{
		user = db->get_user(arg.at(0));
	}
	catch (...) {
		return HTTP::Error::make_json_error_response("Couldn't find user", HTTP::Code::NOT_FOUND);
	}

	const std::string user_url = Instance::instance_host_https(true) + "users/" + user.acct;
	Protocol::ActivityPub::
		to_object(root, "Person",
	  {
		  AP_pair("id", user_url, a),
		  AP_pair("url", user_url, a),
		  AP_pair("name", user.display_name, a),
		  //AP_pair("manuallyApprovesFollowers", false),
		  AP_pair("preferredUsername", user.acct, a),
		  AP_pair("summary", user.bio, a),
		  AP_pair("inbox", user_url + "/inbox", a),
		  AP_pair("outbox", user_url + "/outbox", a),
		  AP_pair("featured", user_url + "/collections/featured", a),
		  AP_pair("followers", user_url + "/followers", a),
		  AP_pair("following", user_url + "/following", a),
		  AP_pair("liked", user_url + "/liked", a),
	  }, a);
	
	// Keys
	if (!user.key.empty())
	{
		rjson::Value public_key(rjson::kObjectType);
		
		public_key.AddMember("id", user_url + "#main-key", a);
		public_key.AddMember("owner", user_url, a);
		public_key.AddMember("publicKeyPem", user.key, a);
		
		root.AddMember("publicKey", public_key.Move(), a);
	}

#if 0
	// Images, just for display
	rjson::Value icon(rjson::kObjectType);
	icon.AddMember("type", "Image", a);
	// Temporary
	icon.AddMember("url", "", a);
	root.AddMember("icon", icon, a);
#endif
	
	return HTTP::Response( rjson::to_string(root), HTTP::MIME::ACTIVITY_JSON );
}

void ActivityPub::init_activitypub(HTTP::Server* server)
{
	server->add_route({{HTTP::Request::Type::GET, "/users/:/inbox"}, Route::ActivityPub::user_inbox});
	server->add_route({{HTTP::Request::Type::GET, "/users/:/outbox"}, Route::ActivityPub::user_outbox});
	server->add_route({{HTTP::Request::Type::GET, "/users/:/following"}, Route::ActivityPub::user_following});
	server->add_route({{HTTP::Request::Type::GET, "/users/:/followers"}, Route::ActivityPub::user_followers});
	server->add_route({{HTTP::Request::Type::GET, "/users/:"}, Route::ActivityPub::user});
}
