/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#ifndef MODULE_WEBFINGER
#warning "You SHOULD (think: MUST) build Webfinger support if you want to" \
	" build ActivityPub support. If you don't, then there wont be a way for" \
	" most servers to discover users on your server. If you are simply testing" \
	" ActivityPub routes, then you can safely ignore this message."
#endif

#include "http/httpserver.h"
#include "http/response.h"

namespace Route
{
	namespace ActivityPub
	{
		HTTP::Response user_inbox(std::any& args,
		  const HTTP::Request& req,
		  const HTTP::RequestArgs_t& arg);
		HTTP::Response user_outbox(std::any& args,
		   const HTTP::Request& req,
		   const HTTP::RequestArgs_t& arg);
		HTTP::Response user_following(std::any& args,
			  const HTTP::Request& req,
			  const HTTP::RequestArgs_t& arg);
		HTTP::Response user_followers(std::any& args,
			  const HTTP::Request& req,
			  const HTTP::RequestArgs_t& arg);
		HTTP::Response user(std::any& args,
				const HTTP::Request& req,
				const HTTP::RequestArgs_t& arg);
	}
}

namespace Protocol
{
	namespace ActivityPub
	{
		void init_activitypub(HTTP::Server* server);
	}
}
