/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */


#include <cstddef>
#include <stdexcept>
#include <utility>
#include "oauth.h"
#include "instance/instance.h"
#include "http/mime.h"
#include "http/response.h"
#include "http/responsecode.h"
#include "http/httpserver.h"
#include "http/error_response.h"
#include "http/request.h"
#include "random.h"
#include "types/user.h"
#include "logger.h"
#include "jsonhelper.h"
#include "route_args.h"

using namespace Protocol;

HTTP::Response Route::oauth_authorize(std::any& args,
			  const HTTP::Request& req,
			  const HTTP::RequestArgs_t& arg)
{
	static Random rng{};
	constexpr size_t TOKEN_SIZE = 32;
	
	//DESTRUCT_WORMHOLE_ARGS(args);
	rng.generate_token(TOKEN_SIZE);

	return HTTP::Response{ "{}", HTTP::MIME::JSON };
}

void OAuth::init_oauth(HTTP::Server* server)
{
	server->add_route({{HTTP::Request::Type::GET, "/authorize"}, Route::oauth_authorize});
}
