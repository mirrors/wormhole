/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include "http/httpserver.h"
#include "http/response.h"

namespace Route
{
	HTTP::Response oauth_authorize(std::any& args,
		   const HTTP::Request& req,
		   const HTTP::RequestArgs_t& arg);
}

namespace Protocol
{
	namespace OAuth
	{
		void init_oauth(HTTP::Server* server);
	}
}
