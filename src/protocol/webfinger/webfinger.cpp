/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include "instance/instance.h"
#include "jsonhelper.h"
#include <stdexcept>
#include <utility>
#include "route_args.h"
#include "http/mime.h"
#include "http/response.h"
#include "http/responsecode.h"
#include "types/user.h"
#include "webfinger.h"
#include "stringhelper.h"
#include "http/httpserver.h"
#include "http/request.h"
#include "logger.h"
#include "http/error_response.h"

using namespace Protocol;

namespace
{
	struct Resource {
		std::string&& href;
		std::string&& rel;
		std::string&& type;
		std::string&& tmpl;
	};
	
	rjson::Value make_resource(Resource&& res, rjson::Document::AllocatorType& a)
	{
		rjson::Value resource(rjson::kObjectType);
		resource.AddMember("href", res.href, a);
		resource.AddMember("rel", res.rel, a);
		resource.AddMember("type", res.type, a);
		RJSON_OPTIONAL(res.tmpl, resource.AddMember("template", res.tmpl, a));
		return resource;
	}
}

HTTP::Response Route::webfinger(std::any& args, const HTTP::Request& req, const HTTP::RequestArgs_t& arg)
{
	using namespace std::string_literals;
	
	DESTRUCT_WORMHOLE_ARGS(args);
	
	rjson::Document root(rjson::kObjectType);
	rjson::Document::AllocatorType& a = root.GetAllocator();
	rjson::Value links(rjson::kArrayType);
	
	// Resource to read into
	std::string resource;
	// Stored result of account string, stripped from resource
	std::string acct_str;
	User user;

	try
	{
		resource = std::move(req.param.at("resource").string());
	}
	catch (const std::out_of_range& err) {
		return HTTP::Error::make_json_error_response("Missing resource parameter", HTTP::Code::BAD_REQUEST);
	}

	// Perform multiple different Resource formats
	// TODO separate into functions?
	constexpr std::string_view acct_prefix = "acct:";
	static const std::string acct_suffix = "@" + Config::instance().config.instance.host;
	static const std::string local_host_prefix = Instance::instance_host_https(true) + "user/";
	static const std::string at_prefix = Instance::instance_host_https(true) + "@";
	
	// Standard "acct:" prefix check
	if (!resource.compare(0, acct_prefix.size(), acct_prefix))
	{
		if (StringHelper::ends_with(resource, acct_suffix))
		{
			size_t n = resource.find(acct_suffix);
			resource = resource.substr(0, n);
		}
		acct_str = resource.substr(acct_prefix.size());
	}
	else if (!resource.compare(0, local_host_prefix.size(), local_host_prefix)) // Instance host
		acct_str = resource.substr(local_host_prefix.size());
	else if (!resource.compare(0, at_prefix.size(), at_prefix)) // At prefix
		acct_str = resource.substr(at_prefix.size());
	else 
		return HTTP::Error::make_json_error_response("Invalid prefix", HTTP::Code::METHOD_NOT_ALLOWED);

	try
	{
		user = db->get_user(acct_str);
	}
	catch (...) {
		return HTTP::Error::make_json_error_response("Couldn't find user", HTTP::Code::NOT_FOUND);
	}


	// Add profile page
	links.PushBack(make_resource({Instance::instance_host_https(true) + "user/" + acct_str,
		"http://webfinger.net/rel/profile-page",
		"text/html", {}}, a), a);

	// // Add activity stream
	// links.PushBack(make_resource({Instance::instance_host_https(true) + "user/" + acct_str,
	//	 "self",
	//	 "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"", {}}, a), a);

	// Add self page
	links.PushBack(make_resource({Instance::instance_host_https(true) + "user/" + acct_str,
		"self",
		"application/activity+json", {}}, a), a);
	
	root.AddMember("links", links.Move(), root.GetAllocator());

	root.AddMember("subject", resource, a);
	
	return HTTP::Response( rjson::to_string(root), HTTP::MIME::JSON );
}

void Webfinger::init_webfinger(HTTP::Server* server)
{
	server->add_route({
			{
	HTTP::Request::Type::GET,
	"/.well-known/webfinger",
	HTTP::MIME::JSON,
			}, Route::webfinger});
}
