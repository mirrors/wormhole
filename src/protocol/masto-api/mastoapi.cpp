/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */


#include <stdexcept>
#include <utility>
#include "mastoapi.h"
#include "instance/instance.h"
#include "http/mime.h"
#include "http/response.h"
#include "http/responsecode.h"
#include "http/httpserver.h"
#include "http/error_response.h"
#include "http/request.h"
#include "types/user.h"
#include "logger.h"
#include "jsonhelper.h"
#include "route_args.h"

// Routes to be loaded here
#include "apps.h"
#include "instance.h"

using namespace Protocol;
using namespace std::string_literals;

HTTP::Response
Route::MastoAPI::test(std::any& args,
		  const HTTP::Request& req,
		  const HTTP::RequestArgs_t& arg)
{
	DESTRUCT_WORMHOLE_ARGS(args);

	return HTTP::Response{ "{\"test\":\"succeeded1\"}", HTTP::MIME::JSON };
}

void
MastoAPI::init_masto_api(HTTP::Server* server)
{
	server->add_route({{HTTP::Request::Type::GET, "/api/v1/wormhole_test"}, Route::MastoAPI::test});

	init_masto_apps(server);
	init_masto_instance(server);
}
