/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include "instance.h"

#include <utility>
#include "http/response.h"
#include "jsonhelper.h"
#include "route_args.h"
#include <version.h>
#include "http/mime.h"

HTTP::Response
Route::MastoAPI::get_instance(std::any& args,
		 HTTP::Request& req,
		 HTTP::RequestArgs_t const& arg)
{
	DESTRUCT_WORMHOLE_ARGS(args);
	try_load_args_from_json(req);

	rjson::Document root(rjson::kObjectType);
	rjson::Document::AllocatorType& a = root.GetAllocator();

	Config_t&& cfg = db->get_config();

	root.AddMember("approval_required", cfg.account.approval_required, a);
	// Avatar limit is special since avatars _should_ be small
	root.AddMember("avatar_upload_limit", cfg.upload_limit.avatar, a);
	root.AddMember("background_upload_limit", cfg.upload_limit.media, a);
	root.AddMember("banner_upload_limit", cfg.upload_limit.media, a);
	// TODO
	root.AddMember("contact_account", "null", a);
	root.AddMember("registrations", cfg.account.registrations_enabled, a);
	root.AddMember("description", cfg.instance.description, a);
	root.AddMember("description_limit", 42069, a);
	root.AddMember("email", cfg.instance.admin_email, a);
	root.AddMember("rules", rjson::Type::kNullType, a);
	root.AddMember("thumbnail", cfg.instance.thumbnail, a);
	root.AddMember("upload_limit", cfg.upload_limit.media, a);
	root.AddMember("title", cfg.instance.name, a);
	root.AddMember("uri", cfg.instance.domain, a);
	// TODO hardcoded for now...
	root.AddMember("version", std::string(Wormhole::version_string.data()), a);

	return HTTP::Response{ rjson::to_string(root), HTTP::MIME::JSON };
}

void
Protocol::MastoAPI::init_masto_instance(HTTP::Server* server)
{
	server->add_route({
			HTTP::Request{
	HTTP::Request::Type::GET,
	"/api/v1/instance",
	HTTP::MIME::Mimetypes::JSON,
			},
			Route::MastoAPI::get_instance
		});
}
