/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include "http/httpserver.h"
#include "http/response.h"

namespace Route
{
	namespace MastoAPI
	{
		HTTP::Response test(std::any& args,
				const HTTP::Request& req,
				const HTTP::RequestArgs_t& arg);
	}
}

namespace Protocol
{
	namespace MastoAPI
	{
		void init_masto_api(HTTP::Server* server);
	}
}
