/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#ifndef INSTANCE_H
#define INSTANCE_H
#include "http/httpserver.h"

namespace Route
{
	namespace MastoAPI
	{
		HTTP::Response
		get_instance(std::any &args,
		 HTTP::Request &req,
		 HTTP::RequestArgs_t const &arg);
	}
}


namespace Protocol
{
	namespace MastoAPI
	{
		void
		init_masto_instance(HTTP::Server* server);
	}
}
#endif // INSTANCE_H
