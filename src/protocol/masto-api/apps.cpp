/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include "apps.h"

#include <stdexcept>
#include <utility>
#include "mastoapi.h"
#include "instance/instance.h"
#include "http/mime.h"
#include "http/response.h"
#include "http/responsecode.h"
#include "http/httpserver.h"
#include "http/error_response.h"
#include "http/request.h"
#include "types/user.h"
#include "logger.h"
#include "jsonhelper.h"
#include "route_args.h"
#include "random.h"

using namespace Protocol;
using namespace std::string_literals;

constexpr size_t TOKEN_LENGTH = 48;
constexpr size_t TOKEN_SECRET_LENGTH = 48;

HTTP::Response Route::MastoAPI::create_app(std::any& args,
			 HTTP::Request& req,
			 const HTTP::RequestArgs_t& arg)
{
	static Random rng{};
	DESTRUCT_WORMHOLE_ARGS(args);
	try_load_args_from_json(req);

	rjson::Document root(rjson::kObjectType);
	rjson::Document::AllocatorType& a = root.GetAllocator();

	// Required parameters
	try {
		const std::string& client_name =
			req.param.at("client_name").string();
		const std::string& redirect_uris =
			req.param.at("redirect_uris").string();
		
		root.AddMember("name", client_name, a);
		root.AddMember("redirect_uri", redirect_uris, a);
	}
	catch (const std::out_of_range& err)
	{
		return HTTP::Error::make_json_error_response("Missing required parameters", HTTP::Code::BAD_REQUEST);
	}

	// TODO scopes, bullshit aka for security freaks
	try {
		const std::string& website = req.param.at("website").string();
		root.AddMember("website", website, a);
	} catch(...) {
		root.AddMember("website", rjson::Value(), a);
	}
	// Tokens
	const std::string& client_id = rng.generate_token(TOKEN_LENGTH);
	const std::string& client_secret = rng.generate_token(TOKEN_SECRET_LENGTH);

	root.AddMember("id", 0, a);
	root.AddMember("client_id", client_id, a);
	root.AddMember("client_secret", client_secret, a);
	// Not interested in this yet.
	//root.AddMember("vapid_key", client_secret, a);

	return HTTP::Response{ rjson::to_string(root), HTTP::MIME::JSON };
}

void Protocol::MastoAPI::init_masto_apps(HTTP::Server* server)
{
	server->add_route({
			{
	HTTP::Request::Type::POST,
	"/api/v1/apps",
	HTTP::MIME::Mimetypes::ANY
			}, Route::MastoAPI::create_app});
}
