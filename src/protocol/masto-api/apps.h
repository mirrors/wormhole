/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#ifndef APPS_H
#define APPS_H
#include "http/httpserver.h"
#include "http/response.h"

namespace Route
{
	namespace MastoAPI
	{
		HTTP::Response
		create_app(std::any &args,
		HTTP::Request &req,
		HTTP::RequestArgs_t const& arg);
	}
}

namespace Protocol
{
	namespace MastoAPI
	{
		void
		init_masto_apps(HTTP::Server* server);
	}
}

#endif // APPS_H
