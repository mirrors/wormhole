/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include "http/httpserver.h"
#include "http/response.h"

namespace Route
{
	HTTP::Response host_meta(std::any& args, const HTTP::Request& req, const HTTP::RequestArgs_t& arg);
}

namespace Protocol
{
	namespace HostMeta
	{
		void init_host_meta(HTTP::Server* server);
	}
}
