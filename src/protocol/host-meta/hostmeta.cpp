/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */


#include <stdexcept>
#include <utility>
#include "instance/instance.h"
#include "hostmeta.h"

using namespace Protocol;


HTTP::Response Route::host_meta(std::any& args, const HTTP::Request& req, const HTTP::RequestArgs_t& arg)
{
	using namespace std::string_literals;
	
	//DESTRUCT_WORMHOLE_ARGS(args);

	// Sorry :-)
	return HTTP::Response{
		R"xml(<?xml version="1.0" encoding="UTF-8"?><XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0"><Link rel="lrdd" template=")xml" + Instance::instance_host_https(true) + R"xml(.well-known/webfinger?resource={uri}" type="application/xrd+xml" /></XRD>)xml", HTTP::MIME::XML };
}

void HostMeta::init_host_meta(HTTP::Server* server)
{
	server->add_route({{HTTP::Request::Type::GET, "/.well-known/host-meta"}, Route::host_meta});
}
