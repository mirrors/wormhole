/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

/* See this!!!
 * -------------
 * OpenSSL has deprecated the RSA features (again) for some SSL
 * replacement, but version 3.0 is still fairly new and hasn't even
 * landed in Gentoo or Arch stable, only Debian Testing (lol) seems to
 * have the latest OpenSSL that I know of, which is the system I am
 * typing this on.
 *
 * For now, Wormhole remains compatible with version 1.1.1, and probably
 * for a long while.
 */
#define OPENSSL_NO_DEPRECATED
#define OPENSSL_API_COMPAT 0x10000000L

#include <cstddef>
#include <openssl/rsa.h>
#include <memory>
#include <string>

namespace Crypt
{
	class Rsa
	{
	public:
		Rsa(std::shared_ptr<::RSA> key);
		~Rsa() = default;

		std::string to_string() const;
	private:
		std::shared_ptr<RSA> key;
	};
	
	class RsaFactory
	{
	public:
		// 2048 is considered secure and often used between implementations
		RsaFactory();
		~RsaFactory() = default;

		Rsa generate_key(const size_t bits = 2048) const;
	private:
		
		std::unique_ptr<BIGNUM, decltype(&BN_free)> e;
	};
}
