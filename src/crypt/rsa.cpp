/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include "rsa.h"
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/bn.h>
#include <stdexcept>
#include <cstdlib>
#include <ctime>

using namespace Crypt;

Rsa::Rsa(std::shared_ptr<::RSA> key)
	: key{key}
{}

std::string Crypt::Rsa::to_string() const
{
	BIO* bio = BIO_new(BIO_s_mem());
	PEM_write_bio_RSAPrivateKey(bio, key.get(), NULL, NULL, 0, NULL, NULL);
	// The size
	const size_t len = BIO_pending(bio);

	char* data = new char[len + 1];
	BIO_read(bio, data, len);
	data[len] = '\0';

	// Store in string
	// This could be faster done... but for now I don't care
	std::string res{data};
	delete[] data;
	return res;
}

RsaFactory::RsaFactory()
	: e(BN_new(), BN_free)
{
	std::srand(std::time(nullptr));
	if (!BN_set_word(e.get(), RSA_F4))
		throw std::runtime_error(ERR_reason_error_string(ERR_get_error()));
}

Rsa RsaFactory::generate_key(const size_t bits /* = 2048 */) const
{
	int code;
	RSA* rsa = RSA_new();
	code = RSA_generate_key_ex(rsa, bits, e.get(), nullptr);
	if (!code)
		throw std::runtime_error(ERR_reason_error_string(ERR_get_error()));
	
	return Rsa{ std::shared_ptr<::RSA>{rsa, RSA_free} };
}
