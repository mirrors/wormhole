/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include <iostream>
#include <cstdlib>
#include <filesystem>
#include <memory>
#include <any>
#include "control.h"
#include "config/config_loader.h"
#include "database/database.h"
#include "database/sqlite/sqlite.h"
#include "http/httpserver.h"
#include "http/microhttpd_server.h"
#include "http/mime.h"
#include "logger.h"
#include "route_args.h"

// Compile-time modules

/* Each module needs to be loaded via the /CMakeLists.txt file,
   and as such, the module header must exist and be defined */
#ifdef MODULE_WEBFINGER
#  include "protocol/webfinger/webfinger.h"
#endif // MODULE_WEBFINGER
#ifdef MODULE_ACTIVITYPUB
#  include "protocol/activitypub/activitypub.h"
#endif // MODULE_ACTIVITYPUB
#ifdef MODULE_HOST_META
#  include "protocol/host-meta/hostmeta.h"
#endif // MODULE_HOST_META
#ifdef MODULE_OAUTH
#  include "protocol/oauth/oauth.h"
#endif // MODULE_OAUTH
#ifdef MODULE_MASTO_API
#  include "protocol/masto-api/mastoapi.h"
#endif // MODULE_MASTO_API
#ifdef MODULE_FCGI
#  include "frontends/fcgi/fcgi.h"
#endif // MODULE_FCGI

void init_routes(const std::unique_ptr<HTTP::Server>& server)
{
#ifdef MODULE_WEBFINGER
	Protocol::Webfinger::init_webfinger(server.get());
#endif // MODULE_WEBFINGER
#ifdef MODULE_ACTIVITYPUB
	Protocol::ActivityPub::init_activitypub(server.get());
#endif // MODULE_ACTIVITYPUB
#ifdef MODULE_HOST_META
	Protocol::HostMeta::init_host_meta(server.get());
#endif // MODULE_HOST_META
#ifdef MODULE_OAUTH
	Protocol::OAuth::init_oauth(server.get());
#endif // MODULE_OAUTH
#ifdef MODULE_MASTO_API
	Protocol::MastoAPI::init_masto_api(server.get());
#endif // MODULE_MASTO_API

	// Global routes
#ifdef MODULE_FCGI
	Frontend::init_fcgi(server.get());
#endif // MODULE_FCGI
}

int
start_wormhole()
{
	using namespace std::string_literals;
	
	Logger::log("Loading config...");

	std::shared_ptr<DB::Database> database = DB::load_db_from_cfg();

#ifdef MODULE_FCGI
	// Config loaded, fork main FCGI process early if set in config
	Frontend::fork_fcgi_process();
#endif
	
	// Passed as std::any to each route function
	RouteArgs args{database.get()};
	
	// Start HTTPD Server
	std::unique_ptr<HTTP::Server> server =
		std::make_unique<HTTP::MicroHttpdServer>(
			Config::instance().config.http.port,
			std::make_any<RouteArgs>(args)
			);

	database->try_migration();

	init_routes(server);

	// TODO Move to another file
	server->add_routes({
			{ {HTTP::Request::Type::GET, "/"}, [](std::any& args, const HTTP::Request& req, const HTTP::RequestArgs_t& arg){
	// TODO Perl static script to just do this and put it in a header file
	return HTTP::Response{R"html(
<!DOCTYPE html>
<html>
	<head>
		<style>
		 html { height: 100%; }
		 body
		 {
	font-family: Arial, Helvetica, "MS Comic Sans";
	text-align: center;
	background-color: #f8f8f8;
	background: linear-gradient(#dfdfdf 20%, #f8f8f8 80%);
	color: #000;
		 }
		 @media (prefers-color-scheme: dark) {
	body {
	 background-color: #202020;
	 color: #fff;
	}
	a { color: #6cb7f0; }
		 }
		</style>
		<body>
			<h1>Wormhole</h1>
			<p>This is a <a href="http://wormhole.nekobit.net/">Wormhole</a> instance, which is a federated social media server.</p>
			<p>Work in progress :-)</p>
		</body>
	</head>
</html>
)html", HTTP::MIME::HTML};
			} },
		});

	// Start the server! This forks in the background
	server->start();

	Logger::log("HTTP server running on port "s +
	std::to_string(Config::instance().config.http.port));

	/* So... now what?
	 * Well, if we are building with FCGI, we can use this thread
	 * to listen to our pipes */
#ifdef MODULE_FCGI
	Frontend::poll_fcgi_from_process();
#endif
	Logger::log("^D or enter 'q' to stop");
	char c;
	while ((c = getchar()) != EOF && c != 'q' && c != 'Q');
	return EXIT_SUCCESS;
}

inline void
load_configs()
{
	// Initialize config
	// Try XDG config, then HOME
	std::filesystem::path xdgpath;
	if (std::getenv("XDG_CONFIG_HOME"))
		xdgpath = std::getenv("XDG_CONFIG_HOME");
	else if (std::getenv("HOME"))
		xdgpath = std::getenv("HOME");
		
	std::filesystem::path pths[] = {
		"config.yaml",
		xdgpath.generic_string() + "/wormhole/config.yaml",
		"/etc/wormhole/config.yaml"
	};

	bool found = false;
	for (auto path: pths)
		if (!path.empty() && std::filesystem::exists(path))
		{
			Config::instance().load_config(path);
			found = true;
			break; // Config found
		}

	if (!found)
	{
		std::cerr << " !! Couldn't find a config file for Wormhole, "
			"this is required to function. Possible paths include: \n";
		for (auto path: pths)
		{
			std::cerr << "	- " << path.generic_string() << "\n";
		}
		std::cerr << std::endl;
		exit(EXIT_FAILURE);
	}
}


int
main(int argc, char** argv)
{
	int res = EXIT_SUCCESS;

	load_configs();

	// Notify user if in debug mode
#ifndef NDEBUG
	std::cout << " !! Notice: You compiled with " RED " DEBUG MODE " RESET " which "
		"means that some debug features are enabled, like full CORS request access."
	 << std::endl;
		
#endif

	// Start wormhole as is, no arguments needed
	if (argc <= 1)
	{
		res = start_wormhole();
	}
	// User provided an argument, entering control mode...
	else {
		// Convert to string
		return control_mode_init(argc, argv);
	}
	
	return res;
}
