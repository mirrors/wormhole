/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#pragma once

#include <any>
#include <array>
#include "database/database.h"
#include "common.h"

// Apologies if the word "DESTRUCT" is confusing here, it simply means to cast into regular variables
#define DESTRUCT_WORMHOLE_ARGS(_arg) DB::Database* db = std::any_cast<RouteArgs>(_arg).db; \
	HINT_UNUSED(db);				\
	

struct RouteArgs
{
	DB::Database* db;
};
