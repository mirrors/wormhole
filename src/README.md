# File structure

Most large projects tend to lack these, so I'm just putting it here

 - `protocol/` - Anything that has a specification, sort of.
   - Masto-api
   - Litepub
   - webfinger
   - ...
 - `config/` - Anything configuration related on startup
 - `crypt/` - Anything security/hash/randomization/cryptograpy related
 - `database/` - All the database stuff
   - sqlite
   - postgresql
 - `frontends/` - Things like the FCGI reader and static HTML frontends, usually
   pretty ignored
 - `http/` - HTTP stuff
 - `instance/` - Anything related to the instance
 - `types/` - Special, simple types to represent things
   - User
   - Song
   - Status
   - Follow
