/*
 * Wormhole - Federated social network
 * Licensed under BSD 3-Clause. See LICENSE
 */

#include <iostream>
#include "thread_pool.h"

using namespace Threads;

ThreadPool::ThreadPool(size_t size)
	: pool{size},
	  it_smallest_pool{pool.begin()}
{}

// Sets `it_smallest_pool' to the smallest pool available (based on ThreadInfo::children)
void ThreadPool::set_smallest_pool()
{
	auto cur_pool = pool.begin();
	size_t count = cur_pool->children;
	
	for (auto i = pool.begin(); i != pool.end(); ++i)
		if (i->children < count)
		{
			cur_pool = i;
			count = i->children;
		}
	
	it_smallest_pool = cur_pool;
}
