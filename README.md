# Wormhole

> Note: Most of these claims are not all valid yet, as Wormhole is
> still heavily in development. This is a placeholder for the README
> and the goals of the project.

Wormhole is a lightweight federated social media server which allows
for efficient communication between ActivityPub servers.

 - **Fast** -- Wormhole is written in C++ and utilizes efficient libraries
   for parsing
 - **Lightweight** -- Incredibly tiny resource usage is possible, with the
   choice of SQLite or PostgreSQL for the backend.
 - **Portable** -- Completely distributable, no need to compile from
   source to change a configuration option or run migrations. Wormhole
   is a self-contained powerful executable.
 - **Secure** -- While we don't use Rust, we utilize kernel-level utilities
   like seccomp on Linux, capsicum on FreeBSD, and Pledge/Unveil on OpenBSD.
 - **Modular and Extensible** -- Designed for easy development and drop-in
   implementations for new protocols, including run-time plugins and
   compile-time modules.
 - **Powerful when needed** -- Nearly every component can be customized,
   from caching, federation thresholds, or even low-end database
   options.
 - **Easy to setup** -- Run the executable
   once to generate a config if not already done. If you don't understand the
   above, you may even be fine :-)
 - **Compatible** -- Bring your own Pleroma/mastodon database, switch
   between SQLite and Postgres, use your favorite client: Wormhole
   remains compatible in as many areas as possible

Of course, you are encouraged to build and try for yourself if you
choose to disregard these claims! **(see note above)**

# Documentation

Wormhole provides thorough documentation regarding each component in
the `docs/` directory. Code is documented with Javadoc and can be
generated with `DoxyGen` (as can the pages in the `docs/` directory);
to do so, run the following command:

    $ doxygen

Doxygen will generate *HTML*, *Docbook*, *XML*, and *LaTeX* files in
the `docs/doxygen` directory. A HTML file that contains a redirect to
the `index.html` file is simply placed for convience.

There are also hand-written man pages that briefly assist a user
without a web browser.

If something is undocumented, please let us know. Poor documentation is
considered a bug.

# Building

Wormhole uses Premake as the primary build system. Create a build
directory, then run premake to generate the build files, followed by
your build mechanism (i.e. GNU Make).

## Build Dependencies names (by package manager)

<!-- - Debian/Ubuntu (apt): `todo` -->
- Void Linux (xbps): `rapidjson openssl-devel libmicrohttpd-devel libcurl-devel sqlite-devel yaml-cpp-devel`
<!-- - Arch (pacman): `todo` -->
<!-- - macOS (brew): `todo` -->
<!-- - Fedora/RHEL (dnf): `todo` -->
<!-- - FreeBSD (pkg): `todo` -->
<!-- - NetBSD (pkg_add): `todo` -->
<!-- - OpenBSD (pkg_add): `todo` -->

## Release mode

	$ mkdir build; cd build
	$ cmake -DCMAKE_BUILD_TYPE=Release ..
	$ make      # use -j# to parallel compile quickly
    
Following the [Installing](#Installing) steps afterwards.
    
## Debug mode

When compiled with debug mode, debug logs will show up by default,
extra asserts will be done, and the program can be debugged.

	$ mkdir build; cd build
	$ cmake -DCMAKE_BUILD_TYPE=Debug ..
	$ make -j2

# Installing

Assuming you've [built](#building) wormhole in Release mode, you can
now install *(as root, of course)*

	# make install
    
Now setup your services or what-nots, and you're done!

# License

Wormhole is licensed under the BSD license.

```
Copyright (c) 2022, Nekobit

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
```
