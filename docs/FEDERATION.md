# Federation

Federation is a form of decentralization, where content is *mirrored*
across a graph of networks. In context of online networks:

 - Centralization is the term for a dedicated, single piece of
   software that is owned by a specific authority or organization.
 - Decentralization means that a service can be hosted by many people,
   and there can even be several implementations readily available.
 - Federation, a form of decentralization, is when many servers
   communicate with each other and retain the information.

Federation with nodes can be best described as a "side effect," as
there isn't truly a defined way on how a server should
federate. Wormhole follows a general approach to federation, trying
it's best to federate with whatever it can, but only federating within
the scope of the content it already knows.

### Webfinger - Discovery protocol

Wormhole uses the [Webfinger protocol][rfc7033] to lookup and discover
users under different networks. Webfinger allows for servers to use
their own external links and expose their own protocols, which allows
Wormhole to discover if a lookup is valid.

Webfinger discovery occurs for Wormhole when:

 - A specific account is tagged with a domain attached.
   *(e.g. @user@server.social hello there!)*
 - The `/api/v2/search` endpoint is called with specific data.
 - Wormhole attempts to get context with an unknown user.
 
TODO

[rfc7033]: https://www.rfc-editor.org/rfc/rfc7033
