# Development notes

**Don't trust these except as a last resort! They are exposed as a "TODO" list,
essentially, and just to jot down random thoughts from me as I go along**

### Needed to build on FreeBSD:

- `setenv PKG_CONFIG_PATH /usr/local/libdata/pkgconfig:/usr/local/lib/pkgconfig`
- `setenv CFLAGS "-I/usr/local/include"`
- `setenv LDFLAGS "-L/usr/local/lib"`

---

If you're developing stuff make sure to copy the config file over
